import axios from 'axios';
import { API_URL } from './../constants/Config'

const callApi = (resources, method, body) => {
    return axios({
        method: method,
        url: `${API_URL}/${resources}`,
        data: body
    })
}
export default callApi;


