import React, { Component } from 'react';
import { connect } from 'react-redux';
import moment from "moment";
import { fetchAllProjectReq, newProjectReq } from "./../../actions/projectAction";

class FilePage extends Component {
  constructor(props){
    super(props);
    this.state = {
       
    }
  }
  componentWillMount() {
    if(this.props.project.AllProjects.length < 0){
      this.props.fetchAllProject();
    }    
  }
  render() {
    return (
      <div>AAA</div>
    );
  }
}
const mapDispatchtoProps = (dispatch, props) => {
  return {
    fetchAllProject: () => {
      dispatch(fetchAllProjectReq())
    },
    onNewProject: (project) => {
      dispatch(newProjectReq(project))
    }
  }
}
export default connect(mapStateToProps, mapDispatchtoProps)(FilePage);
