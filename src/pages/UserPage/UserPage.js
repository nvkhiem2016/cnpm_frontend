import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Row, Col, Tabs, Icon } from 'antd';
import UserList from '../../components/users/UserList';
import NewUser from '../../components/users/NewUser';
import { createUserReq } from '../../actions/userAction';

const { TabPane } = Tabs;
class UserPage extends Component {
  constructor(props) {
    super(props)
    this.state = {

    }
  }
  onNewUser = (user)=>{
    this.props.createUserReq(user);
  }
  render() {
    return (
      <div className="mybody">
        <Tabs defaultActiveKey="1">
          <TabPane
            tab={
              <span>
                <Icon type="user" />
                User List
              </span>
            }
            key="1"
          >
            <UserList
              users={this.props.users.users}
              onNewUser={this.onNewUser}
            />
          </TabPane>
          <TabPane
            tab={
              <span>
                <Icon type="android" />
                Tab 2
              </span>
            }
            key="2"
          >
            Tab 2
          </TabPane>
        </Tabs>

      </div>
    );
  }
}
const mapStateToProps = state => ({
  users: state.users,
});
const mapDispatchtoProps = (dispatch, props) => {
  return {
    createUserReq:(user)=>{
      dispatch(createUserReq(user))
    }
  }
}
export default connect(mapStateToProps, mapDispatchtoProps)(UserPage);

