import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Tabs, Icon } from 'antd'
import { fetchTasksReq, editTask, newTaskReq, editTaskReq, editStatusTaskReq, finishTaskReq } from "./../../actions/taskAction";
import { fetchMyProjectReq } from "./../../actions/projectAction";

import TaskList from '../../components/tasks/TaskList';
import PerformanceTask from '../../components/tasks/PerformanceTask';

import { fetchReminderReq, newReminderReq, editReminderReq } from '../../actions/reminderAction';
import { newCommentReq, fetchAllCommentReq } from '../../actions/commentAction';
import { fetchAllHistoryReq, newHistoryReq } from '../../actions/historyAction';
import { downloadFileReq } from '../../actions/fileAction';

const styleSpin = {
  'text-align': 'center',
  'background': 'rgba(0, 0, 0, 0.05)',
  'border-radius': '4px',
  'margin-bottom': '20px',
  'padding': '30px 50px',
  'margin': '20px 0',
}
const { TabPane } = Tabs
class TaskPage extends Component {
  constructor(props) {
    super(props)
    this.state = {
      timeout: null
    }
  }
  componentDidMount() {

  }
  componentWillUnmount() {
  }
  componentWillMount() {
    this.props.fetchTasks();
    //this.props.fetchReminderReq();
    this.props.fetchMyProject();
  }
  getEditTask = (task) => {
    this.props.editTask(task);
    this.props.fetchAllCommentReq(task.id); //get list comment by Idtask
    this.props.fetchAllHistoryReq(task.id); //get list history by Idtask
  }
  onNewTask = (task) => {
    this.props.newTaskReq(task);
  }
  equalTwoObject = (obj1, obj2) => {
    let result = [];
    let name1 = Object.keys(obj1);
    let name2 = Object.keys(obj2);
    for (let i = 0; i < name2.length; i++) { //name2.length < name1.length
      let propName = name2[i];
      if (obj2[propName] != obj1[propName]) {
        result.push({
          updateDisposition: propName.toUpperCase(),
          newData: obj2[propName],
          oldData: obj1[propName],
          id: obj2["id"]
        })
      }
    }
    return result;
  }
  onEditTask = (task1, task2) => {
    this.props.editTaskReq(task2);//Lưu ý : Khi Edit thành công thì phải tắt Drawer r mở lại, nếu k nó sẽ lưu lại lịch sử cũ.
    //new history ở đây   
    let arr = this.equalTwoObject(task1, task2)
    arr.map(item => {
      this.props.newHistoryReq(item);

    })
  }
  onEditStatus = (task, status) => {
    this.props.editStatusTaskReq(task, status)
    if (status === "FINISH") {
      this.props.finishTaskReq(task)
    }
    //New history, viết z cho đồng bộ vs editTask
    task.updateDisposition = "STATUS";
    task.newData = status;
    task.oldData = task.status;
    this.props.newHistoryReq(task);

  }
  onNewReminder = (reminder) => {
    this.props.newReminderReq(reminder)
  }
  onEditReminder = (reminder) => {
    this.props.editReminderReq(reminder)
  }
  onNewComment = (comment) => {
    this.props.newCommentReq(comment)
  }
  onDownloadFile = (data) => {
    this.props.downloadFile(data)
  }
  render() {

    return (
      <div className="mybody">
        <Tabs defaultActiveKey="1">
          <TabPane
            tab={
              <span>
                <Icon type="snippets" />
                Task List
              </span>
            }
            key="1">
            <TaskList
              auth={this.props.auth.user}
              tasks={this.props.tasks.tasks}
              users={this.props.users.users}
              reminder={this.props.reminder.reminder}
              MyProjects={this.props.project.MyProject}
              AllProjects={this.props.project.AllProjects}
              onGetEditTask={this.getEditTask}
              onNewTask={this.onNewTask}
              onEditTask={this.onEditTask}
              onEditStatus={this.onEditStatus}
              onNewReminder={this.onNewReminder}
              onEditReminder={this.onEditReminder}
              onNewComment={this.onNewComment}
              onDownloadFile={this.onDownloadFile}
            />
          </TabPane>
          <TabPane
            tab={
              <span>
                <Icon type="android" />
                Performance
              </span>
            }
            key="2"
          >
            <PerformanceTask
              auth={this.props.auth.user}
              tasks={this.props.tasks.tasks}
              MyProjects={this.props.project.MyProject}
            />
          </TabPane>
        </Tabs>
      </div>

    );


  }
}
const mapStateToProps = state => ({
  auth: state.auth,
  tasks: state.tasks,
  project: state.project,
  users: state.users,
  reminder: state.reminder,
  //user: state.auth.user,
  socket: state.socket
});
const mapDispatchtoProps = (dispatch, props) => {
  return {
    fetchTasks: () => {
      dispatch(fetchTasksReq())
    },
    fetchMyProject: () => {
      dispatch(fetchMyProjectReq())
    },
    editTask: (task) => {
      dispatch(editTask(task))
    },
    newTaskReq: (newTask) => {
      dispatch(newTaskReq(newTask))
    },
    editTaskReq: (task) => {
      dispatch(editTaskReq(task))
    },
    editStatusTaskReq: (task, status) => {
      dispatch(editStatusTaskReq(task, status))
    },
    fetchReminderReq: () => {
      dispatch(fetchReminderReq())
    },
    newReminderReq: (newReminder) => {
      dispatch(newReminderReq(newReminder))
    },
    editReminderReq: (editReminder) => {
      dispatch(editReminderReq(editReminder))
    },
    newCommentReq: (comment) => {
      dispatch(newCommentReq(comment))
    },
    fetchAllCommentReq: (idTask) => {
      dispatch(fetchAllCommentReq(idTask))
    },
    newHistoryReq: (history, status) => {
      dispatch(newHistoryReq(history, status))
    },
    fetchAllHistoryReq: (idTask) => {
      dispatch(fetchAllHistoryReq(idTask))
    },
    downloadFile: (id) => {
      dispatch(downloadFileReq(id))
    },
    finishTaskReq: (task, status) => {
      dispatch(finishTaskReq(task, status))
    },
  }
}
export default connect(mapStateToProps, mapDispatchtoProps)(TaskPage);

