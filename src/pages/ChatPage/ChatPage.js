import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Row, Col, Icon, Button, Avatar } from '../../../node_modules/antd';
import 'react-chat-elements/dist/main.css';
import {
  MessageList, ChatList,
  SystemMessage,
  SideBar,
  Navbar,
  SpotifyMessage,
  Input
} from 'react-chat-elements'
class ChatPage extends Component {
  constructor(props) {
    super(props)
    this.state = {
      socket: null
    }
  }
  componentDidMount() {

  }
  componentDidUpdate(prevProps, prevState) {

  }
  componentWillReceiveProps(nextProps) {
    this.setState({
      socket: nextProps.socket
    })

  }
  showListUsers = (users)=>{
    users = users.map(user => {
      return {
        avatar: 'https://www.facebook.com/photo.php?fbid=980206698827936&set=a.102794866569128&type=3&theater',
        alt: user.name,
        title: user.name,
        socketid:user.socketid,
        subtitle: 'What are you doing?',
        date: new Date(),
        unread: 3,
      }
    })
    return users;
  }

  render() {
    let { users } = this.props.users;
    return (
      <Row>
        <Col span={6} >
          <ChatList
            className='chat-list'
            dataSource={this.showListUsers(users)}
          />
        </Col>
        <Col span={16} style={{ maxHeight: 400, height: 400 }}>
          <Navbar
            left={
              <div>
                <Avatar style={{ verticalAlign: 'middle' }} size="large">
                  Khiem NV
                </Avatar>

              </div>
            }
            right={
              <div>
                <Button type="dashed" shape="circle" icon="phone" />
                <Button type="dashed" shape="circle" icon="video-camera" />
                <Button type="dashed" shape="circle" icon="info-circle" />
              </div>
            } />

          <MessageList
            className='message-list'
            lockable={true}
            toBottomHeight={'100%'}
            dataSource={[
              {
                position: 'right',
                type: 'text',
                text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit',
                date: new Date()
              },
              {
                position: 'left',
                type: 'text',
                text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit',
                date: new Date()
              }
            ]} />
          <Input
            placeholder="Type here..."
            defaultValue={"AAA"}
            multiline={false}
            leftButtons={
              <div>
                <Button
                  icon="file-add"
                  color='white'
                  backgroundColor='black'
                  text='Send' />
                <Button
                  icon="smile"
                  color='white'
                  backgroundColor='black'
                  text='Send' />
              </div>
            }
            rightButtons={
              <Button
                icon="enter"
                color='white'
                backgroundColor='black'
                text='Send' />
            } />
        </Col>

      </Row>

    );


  }
}
const mapStateToProps = state => ({
  users: state.users,
  socket: state.socket
});
const mapDispatchtoProps = (dispatch, props) => {
  return {

  }
}
export default connect(mapStateToProps, mapDispatchtoProps)(ChatPage);

