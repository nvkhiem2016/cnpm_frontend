import React, { Component } from 'react';
import moment from 'moment'
import socketClient from 'socket.io-client';
import { connect } from 'react-redux';
import jwt_decode from 'jwt-decode';
import './../../App.css'
import store from './../../store/store';
import "antd/dist/antd.css";
import { Layout, Menu, Icon, Input } from 'antd';
import { Switch, Route, BrowserRouter as Router } from "react-router-dom";


import MyMenu from './../../components/MyMenu/MyMenu';
import MyHeader from './../../components/header/MyHeader'
import routes from './../../routes'

import { setCurrentUser, logoutUser } from './../../actions/authActions';
import { clearCurrentProfile } from './../../actions/profileAction';
import { fetchUsersReq } from './../../actions/userAction';
import { fetchAllProjectReq } from "./../../actions/projectAction";
import { fetchTasksReq } from "./../../actions/taskAction";
//socket
import { socketUrl } from './../../constants/socket';
import setAuthToken from './../../utils/setAuthToken';
import { setSocket } from './../../actions/socketAction';
import { fetchReminderReq } from './../../actions/reminderAction';
import openNotification from './../../components/common/Notification';

const {
  Sider, Content
} = Layout;
//const { SubMenu } = Menu;
const Search = Input.Search;

//-----Setlect action
const menu = (
  <Menu onClick={""}>
    <Menu.Item key="1"><Icon type="user" />Set deadline</Menu.Item>
    <Menu.Item key="2"><Icon type="user" />Finish</Menu.Item>
    <Menu.Item key="3"><Icon type="user" />Remove</Menu.Item>
  </Menu>
);
//-------End Select action
if (localStorage.jwtToken) {
  // Set auth token header auth
  setAuthToken(localStorage.jwtToken);
  // Decode token and get user info and exp
  const decoded = jwt_decode(localStorage.jwtToken);
  // Set user and isAuthenticated
  store.dispatch(setCurrentUser(decoded));

  // Check for expired token
  const currentTime = Date.now() / 1000;
  if (decoded.exp < currentTime) {
    // Logout user

    store.dispatch(clearCurrentProfile());
    store.dispatch(logoutUser());
    // TODO: Clear current Profile

    // Redirect to login
    window.location.href = '/';
  }
}
class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      reminder: null,
      setNofitication:[]
    };
  }
  showContentRoute = (routes) => { // show các route 
    let result = null;
    if (routes.length > 0) {
      result = routes.map((route, index) => {
        return <Route
          key={index}
          path={route.path}
          exact={route.exact}
          component={route.main}
        />
      })
    }
    return <Switch>{result}</Switch>
  }
  async componentWillMount() {
    const socket = await socketClient(socketUrl);
    socket.on("connect", () => {
      this.props.setSocket(socket);
    })
    socket.on("disconnect", () => {
      this.props.setSocket({});
    })
    //fetch data
    this.props.fetchUsers();
    this.props.fetchAllProject();
    this.props.fetchTasks();
    this.props.fetchReminderReq();
  }
  componentDidMount() {

  }
  componentDidUpdate() {

  }
  componentWillReceiveProps(nextProp) {
    if (nextProp.reminder.reminder) {
      let {setNofitication} = this.state;
      //set timeout reminder to user  
      setNofitication.forEach(item=>{
        clearTimeout(item);
      })
      
      nextProp.reminder.reminder.forEach((item,index) => {
        if(!item.isRead){
          setNofitication.push(
            setTimeout(() => {
              openNotification({type:"info",nameTask:item.nameTask,description:item.description})
            },moment(item.reminderAt).diff(moment(), 'miliseconds'))
          )
        }
      });
      this.setState({
        setNofitication:setNofitication
      })
    }
  }
  render() {
      return (
        <Router>
          <div>
            <Layout>
              {/*Header*/}
              <MyHeader />
              <Layout>
                <Sider>
                  <MyMenu />
                </Sider>
                <Content>

                  {
                    //TaskList
                    //<Row>.                  
                    this.showContentRoute(routes)
                  }

                </Content>

              </Layout>
            </Layout>
          </div>
        </Router>
      );
    }
  
}
const mapStateToProps = state => ({
  auth: state.auth,
  reminder: state.reminder
});
const mapDispatchToProps = (dispatch, props) => {
  return {
    fetchUsers: () => {
      dispatch(fetchUsersReq())
    },
    fetchAllProject: () => {
      dispatch(fetchAllProjectReq())
    },
    fetchTasks: () => {
      dispatch(fetchTasksReq())
    },
    setSocket: (socket) => {
      dispatch(setSocket(socket))
    },
    fetchReminderReq: () => {
      dispatch(fetchReminderReq())
    },
  }
};
export default connect(mapStateToProps, mapDispatchToProps)(App);

