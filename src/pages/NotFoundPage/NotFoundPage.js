import React, { Component } from 'react';


class NotFoundPage extends Component {
    render() {
        return (
            <div className="container">
                <div class="alert alert-danger">
                    
                    <h1><strong>404!</strong> Không tìm thấy trang {this.props.location}</h1>
                </div>               
            </div>

        );
    }
}

export default NotFoundPage
