import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Calendar, Badge, Row, Col, Alert, Select } from 'antd';
import moment from "moment";
import DetailDate from '../../components/mycalendar/DetailDate';
import { setListTask } from '../../actions/calendarAction';
import { editTaskReq, fetchTasksReq } from '../../actions/taskAction';
import { fetchTaskByProjectReq } from "./../../actions/projectAction";
import { newHistoryReq } from '../../actions/historyAction';
const styleSpin = {
  'text-align': 'center',
  'background': 'rgba(0, 0, 0, 0.05)',
  'border-radius': '4px',
  'margin-bottom': '20px',
  'padding': '30px 50px',
  'margin': '20px 0',
}
const { Option } = Select;

class MyCalendar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      listTask: [],
      selectedValue: moment(),
      current: moment(),
      showDetail: false
    }
  }


  getListData = (value) => {
    let { listTask } = this.state;// cần filter theo ngày vd : {1:[{data},2:[{data2}]]}
    let listData;
    switch (value.date()) {
      case 1:
        listData = listTask.filter(task => {
          return moment(task.deadline).format('MM-DD') === moment(value).format('MM-DD')
        }); break;
      case 2:
        listData = listTask.filter(task => {
          return moment(task.deadline).format('MM-DD') === moment(value).format('MM-DD')
        }); break;
      case 3:
        listData = listTask.filter(task => {
          return moment(task.deadline).format('MM-DD') === moment(value).format('MM-DD')
        }); break;
      case 4:
        listData = listTask.filter(task => {
          return moment(task.deadline).format('MM-DD') === moment(value).format('MM-DD')
        }); break;
      case 5:
        listData = listTask.filter(task => {
          return moment(task.deadline).format('MM-DD') === moment(value).format('MM-DD')
        }); break;
      case 6:
        listData = listTask.filter(task => {
          return moment(task.deadline).format('MM-DD') === moment(value).format('MM-DD')
        }); break;
      case 7:
        listData = listTask.filter(task => {
          return moment(task.deadline).format('MM-DD') === moment(value).format('MM-DD')
        }); break;
      case 8:
        listData = listTask.filter(task => {
          return moment(task.deadline).format('MM-DD') === moment(value).format('MM-DD')
        }); break;
      case 9:
        listData = listTask.filter(task => {
          return moment(task.deadline).format('MM-DD') === moment(value).format('MM-DD')
        }); break;
      case 11:
        listData = listTask.filter(task => {
          return moment(task.deadline).format('MM-DD') === moment(value).format('MM-DD')
        }); break;
      case 12:
        listData = listTask.filter(task => {
          return moment(task.deadline).format('MM-DD') === moment(value).format('MM-DD')
        }); break;
      case 13:
        listData = listTask.filter(task => {
          return moment(task.deadline).format('MM-DD') === moment(value).format('MM-DD')
        }); break;
      case 14:
        listData = listTask.filter(task => {
          return moment(task.deadline).format('MM-DD') === moment(value).format('MM-DD')
        }); break;
      case 15:
        listData = listTask.filter(task => {
          return moment(task.deadline).format('MM-DD') === moment(value).format('MM-DD')
        }); break;
      case 16:
        listData = listTask.filter(task => {
          return moment(task.deadline).format('MM-DD') === moment(value).format('MM-DD')
        }); break;
      case 17:
        listData = listTask.filter(task => {
          return moment(task.deadline).format('MM-DD') === moment(value).format('MM-DD')
        }); break;
      case 18:
        listData = listTask.filter(task => {
          return moment(task.deadline).format('MM-DD') === moment(value).format('MM-DD')
        }); break;
      case 19:
        listData = listTask.filter(task => {
          return moment(task.deadline).format('MM-DD') === moment(value).format('MM-DD')
        }); break;
      case 20:
        listData = listTask.filter(task => {
          return moment(task.deadline).format('MM-DD') === moment(value).format('MM-DD')
        }); break;
      case 21:
        listData = listTask.filter(task => {
          return moment(task.deadline).format('MM-DD') === moment(value).format('MM-DD')
        }); break;
      case 22:
        listData = listTask.filter(task => {
          return moment(task.deadline).format('MM-DD') === moment(value).format('MM-DD')
        }); break;
      case 23:
        listData = listTask.filter(task => {
          return moment(task.deadline).format('MM-DD') === moment(value).format('MM-DD')
        }); break;
      case 24:
        listData = listTask.filter(task => {
          return moment(task.deadline).format('MM-DD') === moment(value).format('MM-DD')
        });
        break;
      case 25:
        listData = listTask.filter(task => {
          return moment(task.deadline).format('MM-DD') === moment(value).format('MM-DD')
        });
        break;
      case 26:
        listData = listTask.filter(task => {
          return moment(task.deadline).format('MM-DD') === moment(value).format('MM-DD')
        });
        break;
      case 27:
        listData = listTask.filter(task => {
          return moment(task.deadline).format('MM-DD') === moment(value).format('MM-DD')
        });
        break;
      case 28:
        listData = listTask.filter(task => {
          return moment(task.deadline).format('MM-DD') === moment(value).format('MM-DD')
        });
        break;
      case 29:
        listData = listTask.filter(task => {
          return moment(task.deadline).format('MM-DD') === moment(value).format('MM-DD')
        });
        break;
      case 30:
        listData = listTask.filter(task => {
          return moment(task.deadline).format('MM-DD') === moment(value).format('MM-DD')
        });
        break;
      case 31:
        listData = listTask.filter(task => {
          return moment(task.deadline).format('MM-DD') === moment(value).format('MM-DD')
        });
        break;
      default: {
        break;
      }
    }
    return listData || [];
  }
  onSelect = (selectedValue) => {
    let { listTask } = this.state;
    //console.log("onSelect ", value)
    this.setState({
      showDetail: true,
      selectedValue: selectedValue,
    });
    this.props.setListTask({ listTask, selectedValue });
  }
  isStatusTask = (task) => {
    if (task.status === "START") {
      return "processing";
    } else if (task.status === "FINISH") {
      return "success";
    } else {
      return "error";
    }
  }
  dateCellRender = (value) => {
    //console.log("dateCellRender ",moment(value).format('YYYY-MM'))
    const listData = this.getListData(value);
    return (
      <ul className="events">
        {
          listData.map(item => (
            <li key={item._id}>
              <Badge status={this.isStatusTask(item)} text={item.name} />
            </li>
          ))
        }
      </ul>
    );
  }
  onPanelChange = (value) => { // sẽ lấy tháng, setstate theo tháng r sẽ render các task trong tháng đó
    // console.log("onPanelChange ",moment(value).format('YYYY-MM'))
    let { tasks } = this.props.tasks;
    let listTask = null;
    listTask = tasks.filter(task => {
      moment(task.deadline).format('YYYY-MM')
      return moment(task.deadline).format('YYYY-MM') === moment(value).format('YYYY-MM')
    })
    this.setState({
      listTask: listTask
    })
  }
  getMonthData = (value) => {
    if (value.month() === 8) {
      return 1394;
    }
  }

  monthCellRender = (value) => {
    //console.log("monthCellRender ",value )
    const num = this.getMonthData(value);
    return num ? (
      <div className="notes-month">
        <section>{num}</section>
        <span>Backlog number</span>
      </div>
    ) : null;
  }
  componentDidMount() {
    this.onFetchMyTask()
  }
  componentWillReceiveProps(nextProps) {
    let { tasks } = nextProps.tasks;
    let value = this.state.current
    let listTask = null;
    listTask = tasks.filter(task => {
      moment(task.deadline).format('YYYY-MM')
      return moment(task.deadline).format('YYYY-MM') === moment(value).format('YYYY-MM')
    })

    this.setState({
      listTask: listTask
    })
  }
  setStatusForShowDetail = (status) => {
    this.setState({
      showDetail: false
    })
  }
  onFetchTaskProject = (id) => {
    this.props.fetchTaskByProject(id);
  }
  onFetchMyTask = () => {
    this.props.fetchTasks();
  }
  showListProject = (listProject) => {
    listProject = listProject.map((project, index) => {
      return (
        <Option value={project._id} key={index}>{project.name}</Option>

      )
    })
    return listProject;
  }
  onChangeProject = (projectId)=>{
    if(projectId===null){
      this.onFetchMyTask();
    }else{
      this.onFetchTaskProject(projectId);
    }
  }
  equalTwoObject = (obj1, obj2) => {
    let result = [];
    let name1 = Object.keys(obj1);
    let name2 = Object.keys(obj2);
    for (let i = 0; i < name2.length; i++) { //name2.length < name1.length
      let propName = name2[i];
      if (obj2[propName] != obj1[propName]) {
        result.push({
          updateDisposition: propName.toUpperCase(),
          newData: obj2[propName],
          oldData: obj1[propName],
          id: obj2["id"]
        })
      }
    }
    return result;
  }
  onEditTask = (task1,task2)=>{
    this.props.editTaskReq(task2);//Lưu ý : Khi Edit thành công thì phải tắt Drawer r mở lại, nếu k nó sẽ lưu lại lịch sử cũ.
    //new history ở đây   
    let arr = this.equalTwoObject(task1, task2)
    arr.map(item => {
      this.props.newHistoryReq(item);
    })
  }
  render() {
    //console.log(this.state.listTask)
    let { AllProjects } = this.props.projects;
    let { showDetail } = this.state;
    return (
      <div>
        <Row>
          <Col className="col">
            <Select
              onChange={this.onChangeProject}
              placeholder="Please choose the project"
              defaultValue={null}
            >
              <Option value={null} key={-1}>My task</Option>
              {this.showListProject(AllProjects)}
            </Select>
          </Col>
        </Row>
        <Row>
          <Col >
            <DetailDate
              showDetail={showDetail}
              setStatusForShowDetail={this.setStatusForShowDetail}
              onEditTask={this.onEditTask}
            />
            <Alert
              message={`You selected date: ${this.state.selectedValue && this.state.selectedValue.format('YYYY-MM-DD')}`}
            />
            <Calendar
              //style = {{width :'1100px'}}
              dateCellRender={this.dateCellRender}
              //monthCellRender={this.monthCellRender}
              onPanelChange={this.onPanelChange}
              onSelect={this.onSelect}
              width={20}
            />

          </Col>
        </Row>
      </div>
    );
  }
}
const mapStateToProps = state => ({
  tasks: state.tasks,
  projects: state.project,

});
const mapDispatchtoProps = (dispatch, props) => {
  return {
    setListTask: (data) => {
      dispatch(setListTask(data))
    },
    editTaskReq: (task) => {
      dispatch(editTaskReq(task))
    },
    fetchTasks: () => {
      dispatch(fetchTasksReq())
    },
    fetchTaskByProject: (id) => {
      dispatch(fetchTaskByProjectReq(id))
    },
    newHistoryReq: (history, status) => {
      dispatch(newHistoryReq(history, status))
    },
  }
}
export default connect(mapStateToProps, mapDispatchtoProps)(MyCalendar);

