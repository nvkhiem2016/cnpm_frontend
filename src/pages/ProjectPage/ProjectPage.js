import React, { Component } from 'react';
import { Spin,Button} from 'antd';
import { connect } from 'react-redux';
import ProjectList from '../../components/projects/ProjectList';
import { fetchAllProjectReq, newProjectReq } from "./../../actions/projectAction";
import { stat } from 'fs';
import NewProject from '../../components/projects/NewProject';

const styleSpin = {
  'text-align': 'center',
  'background': 'rgba(0, 0, 0, 0.05)',
  'border-radius': '4px',
  'margin-bottom': '20px',
  'padding': '30px 50px',
  'margin': '20px 0',
}

class ProjectPage extends Component {
  constructor(props){
    super(props);
    this.state = {
      newProjectShow:false
    }
  }
  componentWillMount() {
    if(this.props.project.AllProjects.length < 0){
      this.props.fetchAllProject();
    }    
  }
  setStatusForShow=()=>{
    this.setState({
      newProjectShow:false
    })
  }
  onNewProject = (project)=>{
    this.props.onNewProject(project)
  }
  render() {
    let { project } = this.props;
    if (project.AllProjects.length > 0) {
      return (
        <div >
          <Button type="primary" className="my-btn" size="default" onClick={() => this.setState({ newProjectShow: true })}>New Project</Button>
          <NewProject 
            show={this.state.newProjectShow}
            setStatusForShow={this.setStatusForShow}
            listUser ={this.props.listUser}
            user = {this.props.user}
            onNewProject ={this.props.onNewProject}
          />
          <ProjectList projects={this.props.project.AllProjects} listUser={this.props.listUser}>

            
          </ProjectList >
        </div>

      );
    } else {
      return(<div style={styleSpin}>
        <Spin />
      </div>)
    }

  }
}
const mapStateToProps = state => ({
  project: state.project,
  listUser : state.users.users,
  user: state.auth.user,

});
const mapDispatchtoProps = (dispatch, props) => {
  return {
    fetchAllProject: () => {
      dispatch(fetchAllProjectReq())
    },
    onNewProject : (project)=>{
      dispatch(newProjectReq(project))
    }
  }
}
export default connect(mapStateToProps, mapDispatchtoProps)(ProjectPage);

