import React, { Component } from 'react';
import { Spin, Row, Col, Tabs, Select, Icon, Upload, Button } from 'antd';
import { connect } from 'react-redux';
import { addUserProjectReq, fetchTaskByProjectReq } from '../../actions/projectAction';
import { fetchTasksReq, editTask, newTaskReq, editTaskReq, editStatusTaskReq, finishTaskReq } from "./../../actions/taskAction";
import { newCommentReq, fetchAllCommentReq } from '../../actions/commentAction';

import OwnerProject from '../../components/projects/OwnerProject';
import TaskProject from '../../components/projects/TaskProject';
import { newHistoryReq, fetchAllHistoryReq } from '../../actions/historyAction';
import FileList from '../../components/file/FileList';
import ContainerProject from '../../components/projects/ContainerProject';
import { newFileReq, fetchFileByProjectReq, downloadFileReq } from '../../actions/fileAction';
const styleSpin = {
  'text-align': 'center',
  'background': 'rgba(0, 0, 0, 0.05)',
  'border-radius': '4px',
  'margin-bottom': '20px',
  'padding': '30px 50px',
  'margin': '20px 0',
}
const { Option } = Select;
const { TabPane } = Tabs;

class ProjectItemPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      project: {},
      addUser: []
    }
  }
  componentWillMount() {
    let { match } = this.props;
    let id = match.params.id ? match.params.id : '';
    this.props.fetchTaskByProject(id);
    //fetch all file in project 
    this.props.fetchFileByProject(id);
  }
  componentWillReceiveProps(nextProps) {
    let { match, AllProjects } = this.props;
    let id = match.params.id ? match.params.id : '';
    if (nextProps.AllProjects) {
      if (AllProjects.length > 0) {
        let project = null;
        project = AllProjects.filter(item => {
          return item._id === id;
        })
        this.setState({
          project: project[0]
        })
      }
    }
  }
  componentDidMount() {
    let { match, history, AllProjects } = this.props;
    let id = match.params.id ? match.params.id : '';

    if (AllProjects.length > 0) {
      let project = null;
      project = AllProjects.filter(item => {
        return item._id === id;
      })
      this.setState({
        project: project[0],
        idProject: id
      })
    } else {
      history.goBack();
    }

  }
  showListUser = (listUser) => {
    listUser = listUser.map((user, index) => {
      return (
        <Option value={user._id} key={index} name={user.name} onChange={value => {
          console.log(value)
        }}>{user.name}</Option>
      )
    })
    return listUser;
  }

  onAddUser = (id, addUser) => {
    this.props.onAddUser(id, addUser)
  }
  onGetEditTask = (task) => {
    this.props.editTask(task)
    this.props.fetchAllComment(task.id); //get list comment by Idtask
    this.props.fetchAllHistoryReq(task.id); //get list history by Idtask
  }
  onNewTask = (task) => {
    this.props.newTaskReq(task);
  }
  equalTwoObject = (obj1, obj2) => {
    let result = [];
    let name1 = Object.keys(obj1);
    let name2 = Object.keys(obj2);
    for (let i = 0; i < name2.length; i++) { //name2.length < name1.length
      let propName = name2[i];
      if (obj2[propName] != obj1[propName]) {
        result.push({
          updateDisposition: propName.toUpperCase(),
          newData: obj2[propName],
          oldData: obj1[propName],
          id: obj2["id"]
        })
      }
    }
    return result;
  }
  onEditTask = (task1, task2) => {
    this.props.editTaskReq(task2);
    //new history
    let arr = this.equalTwoObject(task1, task2)
    arr.map(item => {
      this.props.newHistoryReq(item);

    })
  }
  onEditStatus = (task, status) => {
    this.props.editStatusTaskReq(task, status);
    if(status==="FINISH") {
      this.props.finishTaskReq(task)
    }
    //New history, viết z cho đồng bộ vs editTask
    task.updateDisposition = "STATUS";
    task.newData = status;
    task.oldData = task.status;
    this.props.newHistoryReq(task);
  }
  onNewComment = (comment) => {
    this.props.newCommentReq(comment)
  }
  onNewFile = (file) => {
    this.props.newFileReq(file)
  }
  onDownloadFile = (data) => {
    this.props.downloadFile(data)
  }
  render() {

    let { project, addUser } = this.state;
    let { listUser } = this.props;
    if (Object.keys(project).length > 0) {
      return (
        <Tabs defaultActiveKey="1">
          <TabPane
            tab={<span><Icon type="form" />Tasks</span>} key="1">
            <ContainerProject
              //TaskProject
              //tasks={this.props.project.tasks}            dùng chung taskReducer  
              tasks={this.props.tasks.tasks} 
              users={this.props.users.users}
              AllProjects={this.props.project.AllProjects}
              MyProjects={this.props.project.MyProject}
              user={this.props.user}
              auth = {this.props.auth.user}
              onGetEditTask={this.onGetEditTask}
              onNewTask={this.onNewTask}
              onEditTask={this.onEditTask}
              onEditStatus={this.onEditStatus}
              onNewComment={this.onNewComment}
              //OwnerProject
              project={project}
              listUser={listUser}
              onAddUser={this.onAddUser} />
          </TabPane>
          <TabPane
            tab={<span><Icon type="file" />Performance</span>} key="2">
             ABV
          </TabPane>
          <TabPane
            tab={<span><Icon type="file" />Files</span>} key="3">
            <FileList
              idProject={this.state.idProject}
              tasks={this.props.tasks.tasks}
              files = {this.props.project.files}
              onNewFile={this.onNewFile}
              onDownloadFile={this.onDownloadFile}
            />
          </TabPane>
        </Tabs>
      );
    } else {
      return (<div style={styleSpin}>
        <Spin />
      </div>)
    }

  }
}
const mapStateToProps = state => ({
  AllProjects: state.project.AllProjects,
  listUser: state.users.users,
  tasks: state.tasks,

  project: state.project,
  users: state.users,
  reminder: state.reminder,
  auth:state.auth,

  user: state.auth.user//dùng trong TaskProject, người không chịu trách nhiệm sẽ không change status đc

});
const mapDispatchtoProps = (dispatch, props) => {
  return {
    onAddUser: (id, users) => {
      dispatch(addUserProjectReq(id, users))
    },
    fetchTaskByProject: (id) => {
      dispatch(fetchTaskByProjectReq(id))
    },
    editTask: (task) => {
      dispatch(editTask(task))
    },
    newTaskReq: (newTask) => {
      dispatch(newTaskReq(newTask))
    },
    editTaskReq: (task) => {
      dispatch(editTaskReq(task))
    },
    editStatusTaskReq: (task, status) => {
      dispatch(editStatusTaskReq(task, status))
    },
    newCommentReq: (comment) => {
      dispatch(newCommentReq(comment))
    },
    fetchAllComment: (idTask) => {
      dispatch(fetchAllCommentReq(idTask))
    },
    newHistoryReq: (history, status) => {
      dispatch(newHistoryReq(history, status))
    },
    fetchAllHistoryReq: (idTask) => {
      dispatch(fetchAllHistoryReq(idTask))
    },
    newFileReq: (file) => {
      dispatch(newFileReq(file))
    },
    fetchFileByProject: (id) => {
      dispatch(fetchFileByProjectReq(id))
    },
    downloadFile: (data) => {
      dispatch(downloadFileReq(data))
    },
    finishTaskReq: (task) => {
      dispatch(finishTaskReq(task))
    },
  }
}
export default connect(mapStateToProps, mapDispatchtoProps)(ProjectItemPage);

