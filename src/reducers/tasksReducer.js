

import { 
  GET_TASKS,
  NEW_TASK, 
  EDIT_TASK, 
  SET_NULL_EDIT_TASK, 
  EDIT_STATUS_TASK, 
  SET_EDIT_TASK,
  GET_ALL_COMMENT,
  NEW_COMMENT,
  GET_ALL_HISTORY,
  NEW_HISTORY
} 
from './../constants/types';
const findIndex = (value, list) => {
  let index = null;
  index = list.findIndex(item => {
    return item._id === value
  })
  return index;
}
const initialState = {
  tasks: [], 
  editTask: {},
  comments:[],
  histories:[],
  loading: false
};

export default function (state = initialState, action) {
  switch (action.type) {
    case GET_TASKS: {
      //console.log(action.payload)
      return {
        ...state,
        tasks: action.payload,
        
      }
    }
    case NEW_TASK: {
      let { tasks } = state;
      delete action.payload["__v"]
      tasks.push(action.payload)
      return {
        ...state,
        tasks:tasks
      }
    }  
    case SET_EDIT_TASK: {
      let { tasks } = state;
      let index = findIndex(action.payload._id, tasks)
      tasks[index] = action.payload;
      return {
        ...state,
        tasks:tasks
      }
    }
    case EDIT_TASK: {
      //console.log(action.payload)
      return {
        ...state,
        editTask: action.payload
      }
    }
    case EDIT_STATUS_TASK: {
      //console.log(action.payload) 
      state.tasks[action.payload.key].status = action.payload.status;
      return {
        ...state
       
      }
    }
    case SET_NULL_EDIT_TASK: {
      //console.log(action.payload)
      return {
        ...state,
        editTask: action.payload
      }
    }
    case NEW_COMMENT: {
      let { comments } = state;
      comments.push(action.payload)
      return {
        ...state,
        comments: comments
      }
    }
    case NEW_HISTORY: {
      let { histories } = state;
      histories.push(action.payload)
      return {
        ...state,
        histories: histories
      }
    }
    case GET_ALL_COMMENT: {
      return {
        ...state,
        comments: action.payload
      }
    }
    case GET_ALL_HISTORY: {
      return {
        ...state,
        histories: action.payload
      }
    }
    default:
      return state;
  }
}
