import moment from 'moment'
import { SET_REMINDER, PUSH_REMINDER, EDIT_REMINDER, READ_REMINDER } from './../constants/types';

const initialState = {
  reminder: [],
  setNofitication: []
};
const findIndex = (value, list) => {
  let index = null;
  index = list.findIndex(item => {
    return item._id === value
  })
  return index;
}
export default function (state = initialState, action) {
  switch (action.type) {
    case SET_REMINDER: {
      let { reminder } = state;
      reminder = action.payload.map((item, index) => {
        return {
          description: item.description,
          idTask: item.idTask?item.idTask._id:null,
          nameTask: item.idTask?item.idTask.name:"Null",
          idUser: item.idUser._id,
          nameUser: item.idUser.name,
          isRead: item.isRead,
          reminderAt: item.reminderAt,
          _id: item._id
        }
      })
      return {
        ...state,
        reminder: reminder
      };
    }
    case PUSH_REMINDER: {
      let { reminder } = state;
      let temp = {
        description: action.payload.description,
        idTask: action.payload.idTask?action.payload.idTask._id:null,
        nameTask: action.payload.idTask?action.payload.idTask.name:"Null",
        idUser: action.payload.idUser._id,
        nameUser: action.payload.idUser.name,
        isRead: action.payload.isRead,
        reminderAt: action.payload.reminderAt,
        _id: action.payload._id
      }
      reminder.push(temp)
      return {
        ...state,
        reminder: reminder
      };
    }
    case EDIT_REMINDER: {
      let { reminder } = state;
      let index = findIndex(action.payload._id, reminder)
      let temp = {
        description: action.payload.description,
        idTask: action.payload.idTask?action.payload.idTask._id:null,
        nameTask: action.payload.idTask?action.payload.idTask.name:"Null",
        idUser: action.payload.idUser._id,
        nameUser: action.payload.idUser.name,
        isRead: action.payload.isRead,
        reminderAt: action.payload.reminderAt,
        _id: action.payload._id
      }
      reminder[index] = temp;
      return {
        ...state,
        reminder: reminder
      };
    }
    case READ_REMINDER: {

      let { reminder } = state;
      let index = findIndex(action.payload._id, reminder)

      reminder[index] = {
        ...action.payload,
        isRead:true
      };
      return {
        ...state,
        reminder: reminder
      };
    }
    default:
      return state;
  }
}
