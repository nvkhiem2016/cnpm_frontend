import { combineReducers } from 'redux';
import authReducer from './authReducer'
import tasksReducer from './tasksReducer'
import profileReducer from './profileReducer'
import projectReducer from './projectReducer'
import userReducer from './userReducer'
import errorReducer from './errorReducer'
import calendarReducer from './calendarReducer';
import reminderReducer from './reminderReducer';

import socketReducer from './socketReducer';

const appReducers = combineReducers({
    auth:authReducer,
    errors:errorReducer,
    tasks:tasksReducer,
    profile:profileReducer,
    project:projectReducer,
    users:userReducer,
    calendar:calendarReducer,
    reminder:reminderReducer,
    socket:socketReducer
});
export default appReducers;