import { GET_USERS, ADD_USER_ONLINE, REMOVE_USER_ONLINE, CREATE_USER } from './../constants/types';

const initialState = {
  users: [],
  usersOnline: []
};
const findIndex = (value, list) => {
  let index = null;
  index = list.findIndex(item => {
    return item._id === value
  })
  return index;
}
export default function (state = initialState, action) {
  switch (action.type) {
    case GET_USERS: {
      return {
        ...state,
        users: action.payload
      };
    }
    case CREATE_USER: {
      let { users } = state;
      users.push(action.payload)
      return {
        ...state,
        users: users
      };
    }
    case ADD_USER_ONLINE: {
      let { usersOnline } = state;
      //let index = findIndex(action.payload.socketid,users)
      usersOnline.push(action.payload)
      return {
        ...state,
        usersOnline: usersOnline
      };
    }
    case REMOVE_USER_ONLINE: {
      let { usersOnline } = state;
      let index = findIndex(action.payload.socketid,usersOnline)
      usersOnline.splice(index,1)
      return {
        ...state,
        usersOnline: usersOnline
      };
    }
    default:
      return state;
  }
}
