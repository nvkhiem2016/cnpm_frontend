

import { 
  GET_MY_PROJECT, 
  GET_ALL_PROJECT, 
  NEW_PROJECT, 
  ADD_USER_PROJECT, 
  GET_TASKS_PROJECT, 
  GET_FILES,
  NEW_FILE} from './../constants/types';

const initialState = {
  AllProjects: [],
  MyProject: [],
  tasks:[],
  files:[],
  loading: false
};
const findIndex = (value, project) => {
  let index = null;
  index = project.findIndex(item => {
    return item._id === value
  })
  return index;
}
export default function (state = initialState, action) {
  switch (action.type) {
    case GET_ALL_PROJECT: {
      return {
        ...state,
        AllProjects: action.payload
      }
    }
    case NEW_PROJECT: {
      let { AllProjects } = state;
      delete action.payload["__v"]
      AllProjects.push(action.payload)
      return {
        ...state,
        AllProjects: AllProjects
      }
    }
    case ADD_USER_PROJECT: {
      let { AllProjects } = state;
      let index = findIndex(action.payload._id, AllProjects)
      AllProjects[index] = action.payload;
      return {
        ...state,
        AllProjects: AllProjects
      }
    }
    case GET_TASKS_PROJECT: {
      return {
        ...state,
        tasks:action.payload
      }
    }
    case GET_MY_PROJECT: {
      return {
        ...state,
        MyProject: action.payload
      }
    }
    case GET_FILES: {
      return {
        ...state,
        files: action.payload
      }
    }
    case NEW_FILE: {
      let { files } = state;
      files.push(action.payload)
      return {
        ...state,
        files: files
      }
    }
    default:
      return state;
  }
}
