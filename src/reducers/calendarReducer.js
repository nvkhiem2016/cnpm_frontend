import isEmpty from '../validation/is-empty';
import moment from "moment";

import { SET_LIST_TASK, SET_DISPLAY_TASK, EDIT_LIST_TASK } from './../constants/types';

const initialState = {
  listTask: [],
  selectedValue: null,
  displayingTask: {}
};
const findIndex = (value, list) => {
  let index = null;
  index = list.findIndex(item => {
    return item._id === value
  })
  return index;
}
export default function (state = initialState, action) {
  switch (action.type) {
    case SET_LIST_TASK: {
      //xử lý code filter
      let { listTask, selectedValue } = action.payload;
      let displayingTask = {}
      listTask = listTask.filter(task => {
        return moment(task.deadline).format('YYYY-MM-DD') === moment(selectedValue).format('YYYY-MM-DD')

      })
      displayingTask = listTask.length > 0 ? listTask[0] : {};
      return {
        ...state,
        selectedValue: selectedValue,
        listTask: listTask,
        displayingTask: displayingTask
      };
    } break;
    case EDIT_LIST_TASK: {
      let { listTask,selectedValue } = state;
      let displayingTask ={}
      let index = findIndex(action.payload._id,listTask)
      listTask[index] = action.payload;
      displayingTask = listTask.length > 0 ? listTask[0] : {};

      return {
        ...state,
        selectedValue,
        displayingTask: displayingTask,
        listTask: listTask
      }
    } break;
    case SET_DISPLAY_TASK: {
      let { listTask } = state;
      let displayingTask ={}
      displayingTask = listTask.find(task=>{
        return task._id ===action.payload._id
      })
      return {
        ...state,
        displayingTask: displayingTask
      }
    } break;
    
    default:
      return state;
  }
}
