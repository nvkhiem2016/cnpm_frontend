import React, { Component } from 'react';
import { connect } from "react-redux";
import Moment from 'react-moment';
import moment from 'moment'
import { logoutUser, setCurrentUser } from "./../../actions/authActions";
import { clearCurrentProfile } from "./../../actions/profileAction";
import { Row, Col, Menu, Icon, Input, Avatar, Dropdown, Layout, Badge, Alert } from 'antd';
import ListNotification from '../reminder/ListNotification';
const Search = Input.Search;
const {
  Header
} = Layout;
class MyHeader extends Component {
  constructor(props) {
    super(props);
    this.state = {
      listNotificationShow : false,

      timenow: moment().format().timenow
    }
  }
  componentWillReceiveProps(nextProps){
    // if(nextProps.reminder){
    //   console.log("header" , nextProps.reminder)

    //   this.setState({
    //     reminder:nextProps.reminder
    //   })
    // }
  }
  onLogout = () => {
    this.props.onLogoutUser();
  }
  setStatusForShow = () => {
    this.setState({
      listNotificationShow:false
    })
  }
  overlayReminder = (listReminder) => {
    let result = null;
    result = listReminder.map((item, index) => {
      return (
        <div>

            <Menu.Item key={index}>
              <Alert
                message={item.nameTask ? item.nameTask : "Reminder"}
                description={item.description}
                type="warning"
              style={{ width: 450 }}
              
                showIcon
              />
            </Menu.Item>

        </div>
      )
    })
    return <Menu>{result}</Menu>;
  }
  countReminder = (reminder)=>{
    let result = null;
    result = reminder.filter(item=>{
      return item.isRead===false
    })
    return result.length;
  }
  render() {
    let {reminder } = this.props;
    return (
      <Header className="header">
        <Row gutter={50} justify="center">
          <Col span={4} className="col">Task Management</Col>
          <Col span={10} className="col">
            <Search
              placeholder="input search text"
              onSearch={value => console.log(value)}
              style={{ width: 450 }}
            />
          </Col>
          <Col span={4} className="col"><Moment format={'h:mm:ss a'} interval={1}>{this.state.timenow}</Moment></Col>
          <Col span={1} className="col">
              <a>
                <Badge count={this.countReminder(reminder)}>
                  <Icon type="bell" style={{ fontSize: '25px', color: '#08c' }} onClick={()=>this.setState({listNotificationShow:true})}/>
                </Badge>
              </a>
              <ListNotification  
                show={this.state.listNotificationShow}
                setStatusForShow ={this.setStatusForShow}
              />
          </Col>

          <Col span={5} className="col">
            <div>

              <Avatar icon="user" />
              <Dropdown overlay={
                <Menu>
                  <Menu.Item key="0">
                    <a href="#">Profile</a>
                  </Menu.Item>
                  <Menu.Divider />
                  <Menu.Item key="3" onClick={this.onLogout}>Logout</Menu.Item>
                </Menu>}
                trigger={['click']}>
                <a className="ant-dropdown-link" href="#">
                  {this.props.user.name} <Icon type="down" />
                </a>
              </Dropdown>

            </div>
          </Col>
        </Row>
      </Header>
    );
  }
}
const mapStateToProps = state => ({
  profile: state.profile.profile,
  user: state.auth.user,
  reminder: state.reminder.reminder
});
const mapDispatchtoProps = (dispatch, props) => {
  return {
    onLogoutUser: () => {
      dispatch(logoutUser())
    }


  }
}
export default connect(mapStateToProps, mapDispatchtoProps)(MyHeader)
