import React, { Component } from 'react';
import { Modal, Button, Row, Col, Empty, Timeline, Icon, Menu } from 'antd';
import { connect } from 'react-redux';
import moment from "moment";
import { setDisplayingTask } from '../../actions/calendarAction';
import DetailForm from './DetailForm';

class DetailDate extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: false,
      displayingTask: {}
    };
  }

  showModal = () => {
    this.setState({
      visible: true,
    });
  };

  handleOk = e => {
    this.props.setStatusForShowDetail(false);
    this.setState({
      visible: false,
    });
  };

  handleCancel = e => {
    this.props.setStatusForShowDetail(false);
    this.setState({
      visible: false,
    });
  };
  componentDidMount() {
    let { listTask } = this.props;
    this.setState({
      displayingTask: listTask[0]
    })
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.showDetail) {
      this.setState({
        visible: nextProps.showDetail
      })
    }

  }
  onClickTask = (_id) => {
    this.props.setDisplayingTask({ _id });
  }
  showListTask = (listTask) => {
    let list = null;
    list = listTask.map((task, index) => {
      return (
        <Menu.Item key={index} onClick={() => this.onClickTask(task._id)}>
          <span>{task.name}</span>
        </Menu.Item>
      )
    })
    return (<Menu
        defaultSelectedKeys={['0']}
        style={{ height: '50%',width:'90%' }}
        >
    {list}
    </Menu>);
  }
  isStatusTask = (task) => {
    if (task.status === "FINISH") {
      return "check";
    } else if (task.status === "START") {
      return "loading"
    } else if (task.status === "PAUSE") {
      return "pause";
    } else if (moment(task.deadline).diff(moment(), 'hours') < 24) {
      return "warning";
    }
  }
  showContent = (listTask, displayingTask, selectedValue) => {
    if (listTask.length > 0) {
      return (
        <Row>
          <Col span={7}>
            {this.showListTask(listTask)}
          </Col>
          <Col span={6}>
            <Timeline>
              <Timeline.Item>{"Start At " + moment(displayingTask.createAt).format('YYYY-MM-DD HH:mm:ss')}</Timeline.Item>
              <Timeline.Item dot={<Icon type="clock-circle-o" style={{ fontSize: '16px' }} />} color="red">
                {/* {moment(displayingTask.deadline).fromNow()} */}
                {displayingTask.status === "FINISH" ? "FINISHED" : moment(displayingTask.deadline).diff(moment(), 'hours') + " hours remaining"}
              </Timeline.Item >
              <Timeline.Item dot={<Icon type={this.isStatusTask(displayingTask)} style={{ fontSize: '16px' }} />} color="red">
                {"Dealine " + moment(displayingTask.deadline).format('YYYY-MM-DD HH:mm:ss')}
              </Timeline.Item >
            </Timeline>
          </Col>
          <Col span={11}>
            {/* {Object.keys(displayingTask).length > 0 ? displayingTask.name:''} */}
            <DetailForm 
              onEditTask = {this.props.onEditTask}
            />
          </Col>
        </Row>
      )
    } else {
      return (<Empty displayingTask={displayingTask}/>)
    }
  }
  render() {
    let { listTask, displayingTask, selectedValue } = this.props;
    //let { displayingTask } = this.state;

    return (
      <div>
        <Modal
          title={listTask.length > 0 ? moment(listTask[0].deadline).format('YYYY-MM-DD') : moment(selectedValue).format('YYYY-MM-DD')}
          visible={this.state.visible}
          onOk={this.handleOk}
          onCancel={this.handleCancel}
          width={"65%"}
          destroyOnClose={true}
        >
          {this.showContent(listTask, displayingTask, selectedValue)}
        </Modal>
      </div>
    );
  }
}
const mapStateToProps = state => ({
  listTask: state.calendar.listTask,
  displayingTask: state.calendar.displayingTask,
  selectedValue: state.calendar.selectedValue


});
const mapDispatchtoProps = (dispatch, props) => {
  return {
    setDisplayingTask: (data) => {
      dispatch(setDisplayingTask(data))
    }
  }
}
export default connect(mapStateToProps, mapDispatchtoProps)(DetailDate);