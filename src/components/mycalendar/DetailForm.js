import React, { Component } from 'react';
import { connect } from "react-redux";
import moment from "moment";
import { Form, Select, Button, Row, Col, DatePicker } from 'antd';
const { Option } = Select;

class DetailForm extends Component {
  OnEditTaskSubmit = e => {
    e.preventDefault();
    let { displayingTask } = this.props;
    let editTask = this.props.displayingTask;

    this.props.form.validateFields((err, values) => {
      editTask.deadline = moment(editTask.deadline).format('YYYY-MM-DD HH:mm');
      editTask.startOn = moment(editTask.startOn).format('YYYY-MM-DD HH:mm');
      editTask.reponsiblePerson = editTask.reponsiblePerson[0]._id;// chua lam đc nhieu user (array thì bên BE bị lỗi History)
      editTask.createBy = editTask.createBy._id;
      editTask.supervisorPerson = editTask.supervisorPerson._id;
      editTask.id = editTask._id;

      if (!err) {
        let task = {
          name: displayingTask.name,
          description: displayingTask.description,
          startOn: moment(values.startOn).format('YYYY-MM-DD HH:mm'),
          deadline: moment(values.deadline).format('YYYY-MM-DD HH:mm'),
          createBy: displayingTask.createBy,
          supervisorPerson: displayingTask.supervisorPerson,
          reponsiblePerson: displayingTask.reponsiblePerson,
          projectId: displayingTask.projectId,
          id: displayingTask._id
        }
        this.props.onEditTask(editTask, task);
      }
    });
  };
  showListUser = (listUser) => {
    listUser = listUser.map((user, index) => {
      return (
        <Option value={user._id} key={index}>{user.name}</Option>
      )
    })
    return listUser;
  }
  render() {
    const { getFieldDecorator } = this.props.form;
    let { listUser, displayingTask } = this.props;
    return (
      <Form onSubmit={this.handleSubmit} className="login-form">
        <Row gutter={16}>
          <Col span={12}>
            <Form.Item label="Start At">
              {getFieldDecorator('startOn', {
                initialValue: moment(displayingTask.createAt, 'YYYY-MM-DD HH:mm'),
                rules: [{ required: false, message: 'Please choose the startOn' }],
              })(
                <DatePicker
                  showTime={{ format: 'HH:mm' }}
                  format="YYYY-MM-DD HH:mm"
                  style={{ width: '100%' }}
                />
              )}
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item label="Deadline">
              {getFieldDecorator('deadline', {
                initialValue: moment(displayingTask.deadline),
                rules: [{ required: true, message: 'Please choose the dateTime' }],
              })(
                <DatePicker
                  showTime={{ format: 'HH:mm' }}
                  format="YYYY-MM-DD HH:mm"
                  style={{ width: '100%' }}

                />
              )}
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={16}>
          {/* <Col span={12}>
            <Form.Item label="Reponsible person">
              {getFieldDecorator('reponsiblePerson', {
                initialValue: displayingTask.reponsiblePerson.map(task => {
                  return task._id
                }),
                rules: [{ required: false, message: 'Please choose the reponsible' }],
              })(
                <Select
                  mode="multiple"
                  placeholder="Please choose the person">
                  {this.showListUser(listUser)}
                </Select>
              )}
            </Form.Item>
          </Col> */}

        </Row>
        <Form.Item>
          <Button type="primary" onClick={this.OnEditTaskSubmit}>
            Save
          </Button>
        </Form.Item>
      </Form>
    );
  }
}
const mapStatetoProps = state => ({
  listUser: state.users.users,
  displayingTask: state.calendar.displayingTask
})
const mapDispatchtoProp = (dispatch, props) => {
  return {

  }
}
export default connect(mapStatetoProps, mapDispatchtoProp)(Form.create({ name: 'detail-form' })(DetailForm));