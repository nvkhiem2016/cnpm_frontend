import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'react-dom'
import {
  Form, Icon, Input, Button, Checkbox, Row, Col
} from 'antd';
import { loginUser } from '../../actions/authActions';
class Login extends Component {
  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, user) => {
      if (!err) {
        this.props.loginUser(user)
      }
    });
  }

  render() {
    const { getFieldDecorator } = this.props.form;
    return (
      <Row justify="center" align="middle">
        <Col span={8} offset={8}>
          <Form onSubmit={this.handleSubmit} className="login-form">
            <Form.Item>
              {getFieldDecorator('email', {
                rules: [{ required: true, message: 'Please input your email!' }],
              })(
                <Input prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Email" />
              )}
            </Form.Item>
            <Form.Item>
              {getFieldDecorator('password', {
                rules: [{ required: true, message: 'Please input your Password!' }],
              })(
                <Input prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} type="password" placeholder="Password" />
              )}
            </Form.Item>
            <Form.Item>
              <p>{this.props.errors.message?this.props.errors.message:''}</p>
              <Button type="primary" htmlType="submit" className="login-form-button">
                Log in
              </Button>

            </Form.Item>
          </Form>
        </Col>

      </Row>
    );
  }
}

const WrappedLogin = Form.create({ name: 'normal_login' })(Login);

const mapStateToProps = state => ({
  auth: state.auth,
  errors: state.errors
});
const mapDispatchtoProps = {
  loginUser
}

export default connect(mapStateToProps,mapDispatchtoProps)(WrappedLogin);