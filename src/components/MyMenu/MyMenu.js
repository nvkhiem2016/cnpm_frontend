import React, { Component } from 'react';
import { Menu, Icon } from 'antd';
import { Route, Link } from 'react-router-dom'
const menus = [
  {
    name: 'Tasks',
    to: '/tasks',
    exact: true,
    icon: "pie-chart"
  },
  {
    name: 'Calendar',
    to: '/calendar',
    exact: false,
    icon: "pie-chart"
  },
  {
    name: 'Projects',
    to: '/projects',
    exact: false,
    icon: "pie-chart"
  }
]
//Tạo custom Menu link
const MenuLink = ({ label, to, activeOnlyWhenExact, icon }) => {
  return (
    <Route
      path={to}
      exact={activeOnlyWhenExact}
      children={({ match }) => {
        //let active = match ? 'active' : '';
        return (
          <Menu.Item key="1">
            <Icon type={icon} />
            <span>{label}</span>
            <Link to={to} />
          </Menu.Item>
        )
      }}
    />
  )
}
class MyMenu extends Component {
  showMenu = (menus) => {
    let result = null;
    if (menus.length > 0) {
      result = menus.map((menu, index) => {
        return (
          <MenuLink
            key={index}
            label={menu.name}
            to={menu.to}
            activeOnlyWhenExact={menu.exact}
            icon={menu.icon}
          />
        )
      })
    }
    return result;
  }
  render() {
    return (
      <Menu
        mode="inline"
        defaultSelectedKeys={['1']}
        defaultOpenKeys={['']}
        style={{ height: '100%' }}
      >
        <Menu.Item key="1">
          <Icon type="line-chart" />
          <span>{"My Tasks"}</span>
          <Link to={"/tasks"} />
        </Menu.Item>
        <Menu.Item key="2">
          <Icon type="calendar" />
          <span>{"Calendar"}</span>
          <Link to={"/calendar"} />
        </Menu.Item>
        <Menu.Item key="3">
          <Icon type="project" />
          <span>{"Projects"}</span>
          <Link to={"/projects"} />
        </Menu.Item>
        <Menu.Item key="4">
          <Icon type="user" />
          <span>{"Users"}</span>
          <Link to={"/users"} />
        </Menu.Item>
        <Menu.Item key="5">
          <Icon type="wechat" />
          <span>{"Chat and Call"}</span>
          <Link to={"/chat"} />
        </Menu.Item>
      </Menu>
    );
  }
}

export default MyMenu;
