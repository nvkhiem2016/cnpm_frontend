import React, { Component } from 'react';
import { connect } from "react-redux";
import { setNullEditTask, editTaskReq } from "./../../actions/taskAction";
import moment from "moment";
import {
  Drawer, Form, Button, Col, Row, Input, Select, DatePicker, Icon, Tabs, Avatar, List, Table
} from 'antd';

const { Option } = Select;
const { TabPane } = Tabs;
class EditTask extends Component {
  constructor(props) {
    super(props);
    this.state = {
      chatShow: false,
      visible: true,
      disable: true

    };
  }

  onClose = () => {
    //this.props.setStatusForShow();
    this.setState({
      visible: true,
      chatShow: false,
      disable: true
    });
  };
  showListUser = (listUser) => {
    listUser = listUser.map((user, index) => {
      return (
        <Option value={user._id} key={index}>{user.name}</Option>
      )
    })
    return listUser;
  }

  
  
 
  

  render() {
    const { getFieldDecorator } = this.props.form;
    



    return (
      <div>
        <Drawer
          title="Edit Task"
          width={100}
          height={100}
          onClose={this.onClose}
          visible={this.state.visible}
          destroyOnClose={true}
          maskClosable={false}
        >
          
        </Drawer>
      </div>
    );
  }
}

//   const App = Form.create()(NewTask);
const mapStatetoProps = state => ({
  listUser: state.users,
})
const mapDispatchtoProp = (dispatch, props) => {
  return {

  }
}
export default connect(mapStatetoProps, mapDispatchtoProp)(Form.create()(EditTask));