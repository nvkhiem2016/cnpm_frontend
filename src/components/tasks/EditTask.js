import React, { Component } from 'react';
import { connect } from "react-redux";
import { setNullEditTask, editTaskReq } from "./../../actions/taskAction";
import moment from "moment";
import {
  Drawer, Form, Button, Col, Row, Input, Select, DatePicker, Icon, Tabs, Avatar, List, Table
} from 'antd';

const { Option } = Select;
const { TabPane } = Tabs;
class EditTask extends Component {
  constructor(props) {
    super(props);
    this.state = {
      newProjectShow: false,
      visible: false,
      disable: true

    };
  }
  componentWillReceiveProps(nextProps) {
    this.setState({
      visible: nextProps.show
    });
  }
  onClose = () => {
    this.props.setStatusForShow();
    this.setState({
      visible: false,
      newProjectShow: false,
      disable: true
    });
  };
  getEmailUserById = (id, listUser) => {
    if (listUser) {
      let result = null;
      result = listUser.find(item => {
        return item._id === id
      })
      return result ? result.email : null;
    } else {
      return null;
    }
  }
  getNameProjectById = (id, listProject) => {
    if (listProject) {
      let result = null;
      result = listProject.find(item => {
        return item._id === id
      })
      return result ? result.name : null;
    } else {
      return null;
    }
  }
  onEditTaskSubmit = e => {
    e.preventDefault();
    let editTask = this.props.editTaskData;
    this.props.form.validateFields((err, values) => {
      editTask.deadline = moment(editTask.deadline).format('YYYY-MM-DD HH:mm');
      editTask.startOn = moment(editTask.startOn).format('YYYY-MM-DD HH:mm');
      editTask.reponsiblePerson = editTask.reponsiblePersonId.join();// chua lam đc nhieu user (array thì bên BE bị lỗi History)
      editTask.createBy = editTask.createById;
      editTask.supervisorPerson = editTask.supervisorPerson._id;
      if (!err) {

        let task = {
          name: values.name,
          description: values.description,
          startOn: moment(values.startOn).format('YYYY-MM-DD HH:mm'),
          deadline: moment(values.deadline).format('YYYY-MM-DD HH:mm'),
          reminder: values.reminder,
          createBy: values.createBy,// theo email
          tag: values.tag,
          supervisorPerson: values.supervisorPerson,
          reponsiblePerson: values.reponsiblePerson,
          projectId: values.projectId,
          id: editTask.id
        }
        // editTask = Object.assign(editTask,task)
        console.log(task)
        this.props.onEditTask(editTask, task);//dùng để so sanh 2 object và lưu lại lihcj sử edit
      }
    });
  };
  onChange = (e) => { //không cho NewProject display
    console.log(`selected ${e}`);
    this.setState({

      newProjectShow: false
    });
  }
  showListUserByEmail = (listUser) => {
    listUser = listUser.map((user, index) => {
      return (
        <Option value={user.email} key={index}>{user.name}</Option>
      )
    })
    return listUser;
  }
  showListUser = (listUser) => {
    listUser = listUser.map((user, index) => {
      return (
        <Option value={user._id} key={index}>{user.name}</Option>
      )
    })
    return listUser;
  }
  showListProject = (listProject) => {

    listProject = listProject.map((project, index) => {
      return (
        <Option value={project._id} key={index}>{project.name}</Option>

      )
    })
    return listProject;
  }
  showListComment = (comments) => {
    return (
      <List
        itemLayout="horizontal"
        dataSource={comments}
        renderItem={comment => (
          <List.Item>
            <List.Item.Meta
              avatar={
                <Avatar
                  icon="user"
                  id={comment.createBy._id}
                  title={comment.createBy.name}
                  style={{
                    marginLeft: "5px",
                    cursor: "pointer"
                  }}
                />
              }
              title={
                <div>
                  <a href="javascript:;">{comment.createBy.name}</a>
                  <span>{"     " + moment(comment.createAt).format('YYYY-MM-DD HH:mm')}</span>
                </div>
              }
              description={comment.content}
            />
          </List.Item>
        )}
      />
    )
  }
  showHistory = (histories) => {
    const columns = [
      {
        title: 'CreateAt',
        dataIndex: 'createAt',
        key: 'createAt',
        render: text => <a href="javascript:;">{moment(text).format('YYYY-MM-DD HH:mm')}</a>,
      },
      {
        title: 'CreateBy',
        dataIndex: 'createBy',
        key: 'createBy',
      },
      {
        title: 'Update Disposition',
        dataIndex: 'updateDisposition',
        key: 'updateDisposition',
      },
      {
        title: 'Content',
        dataIndex: 'content',
        key: 'content',
        render: (text, record) => {
          if (record.updateDisposition === "REPONSIBLEPERSON" || record.updateDisposition === "CREATEBY" || record.updateDisposition === "SUPERVISORPERSON") {
            let { listUser } = this.props;
            let oldData = this.getEmailUserById(text[0], listUser) ? this.getEmailUserById(text[0], listUser) : text[0];
            let newData = this.getEmailUserById(text[1], listUser) ? this.getEmailUserById(text[1], listUser) : text[1];
            return (
              <div>
                <span>{oldData}</span>
                <Icon type="arrow-right" />
                <span>{newData}</span>
              </div>
            )
          } else if (record.updateDisposition === "PROJECTID") {
            let { AllProjects } = this.props;
            let oldData = this.getNameProjectById(text[0], AllProjects) ? this.getNameProjectById(text[0], AllProjects) : text[0];
            let newData = this.getNameProjectById(text[1], AllProjects) ? this.getNameProjectById(text[1], AllProjects) : text[1];
            return (
              <div>
                <span>{oldData}</span>
                <Icon type="arrow-right" />
                <span>{newData}</span>
              </div>
            )
          } else {
            return (
              <div>
                <span>{text[0]}</span>
                <Icon type="arrow-right" />
                <span>{text[1]}</span>
              </div>
            )
          }
        },

      }
    ];
    histories = histories.map((history, index) => {
      return {
        key: index,
        createAt: history.createAt,
        createBy: history.createBy.name,
        updateDisposition: history.updateDisposition,
        content: [history.content[0].old, history.content[0].new],
      }
    })
    return (
      <Table columns={columns} dataSource={histories} />
    )
  }
  onChangeTabs = (key) => {
    // console.log(key);
  }
  onNewCommentSubmit = e => {
    e.preventDefault();
    let editTask = this.props.editTaskData;
    let user = this.props.auth;
    this.props.form.validateFields((err, values) => {
      if (!err && values.comment !== undefined) {
        let comment = {
          createBy: user.sub,
          content: values.comment,
          createAt: moment().format('YYYY-MM-DD HH:mm'),
          idTask: editTask.id
        }
        this.props.onNewComment(comment)
      }
    });
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    const { listUser, editTaskData } = this.props;
    const { name, description, reponsiblePersonId, projectId } = editTaskData;
    const { comments, histories } = this.props.tasks;
    const { visible, disable } = this.state;

    return (
      <div>
        <Drawer
          title="Edit Task"
          width={720}
          height={1000000}
          onClose={this.onClose}
          visible={visible}
          destroyOnClose={true}
        >
          <Form layout="vertical" hideRequiredMark>
            <Row gutter={16}>
              <Col span={24}>
                <Form.Item label="Name">
                  {getFieldDecorator('name', {
                    initialValue: name,
                    rules: [{
                      required: true,
                      message: 'Please enter new task',
                    }],
                  })(<Input placeholder="Please enter user name" disabled={disable} />)}
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={16}>
              <Col span={24}>
                <Form.Item label="Description">
                  {getFieldDecorator('description', {
                    initialValue: description,
                    rules: [
                      {
                        required: false,
                        message: 'please enter  description',
                      },
                    ],
                  })(<Input.TextArea rows={4} placeholder="please enter  description" disabled={disable} />)}
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={16}>
              <Col span={12}>
                <Form.Item label="Start on">
                  {getFieldDecorator('startOn', {
                    rules: [{ required: true, message: 'Please choose the startOn' }],
                    initialValue: moment(editTaskData.startOn),
                  })(
                    <DatePicker
                      showTime={{ format: 'HH:mm' }}
                      format="YYYY-MM-DD HH:mm"
                      style={{ width: '100%' }}
                      disabled={disable}
                    />
                  )}
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item label="Deadline">
                  {getFieldDecorator('deadline', {
                    initialValue: moment(editTaskData.deadline),
                    rules: [{ required: true, message: 'Please choose the dateTime' }],
                  })(
                    <DatePicker
                      disabled={disable}
                      showTime={{ format: 'HH:mm' }}
                      format="YYYY-MM-DD HH:mm"
                      style={{ width: '100%' }}

                    />
                  )}
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={16}>
              <Col span={12}>
                <Form.Item label="Reponsible person">
                  {getFieldDecorator('reponsiblePerson', {
                    initialValue: reponsiblePersonId,
                    rules: [{ required: false, message: 'Please choose the reponsible' }],
                  })(
                    <Select
                      disabled={disable}
                      //mode="multiple"
                      placeholder="Please choose the person">
                      {this.showListUser(listUser)}
                    </Select>
                  )}
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item label="Supervisor Person">
                  {getFieldDecorator('supervisorPerson', {
                    initialValue: editTaskData.supervisorPerson ? editTaskData.supervisorPerson._id : "",
                    rules: [{ required: false, message: 'Please choose the supervisor' }],
                  })(
                    <Select
                      disabled={disable}
                      placeholder="Please choose the person">
                      {this.showListUser(listUser)}
                    </Select>
                  )}
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={16}>
              <Col span={12}>
                <Form.Item label="Create By">
                  {getFieldDecorator('createBy', {
                    rules: [{ required: false, message: 'Please choose the create by' }],
                    initialValue: editTaskData.createById
                  })(
                    <Select
                      disabled={disable}
                      onChange={this.onChange}
                      placeholder="Please choose the create by"
                    >
                      {this.showListUser(listUser)}
                    </Select>
                  )}
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item label="Project">
                  {getFieldDecorator('projectId',
                    {
                      initialValue: projectId
                    })(
                      <Select
                        onChange={this.onChange}
                        disabled={disable}
                        placeholder="Please choose the project"
                      >
                        <Option value={null}>Null</Option>
                        {this.showListProject(this.props.AllProjects)}

                      </Select>
                    )}
                </Form.Item>
              </Col>
              {/* <Col span={12}>
                <Form.Item label="New project">
                  {getFieldDecorator('NewProjects', {

                    rules: [{ required: false, message: 'Please choose the project' }],
                  })(
                    <Button type="primary" onClick={() => this.setState({ newProjectShow: true })}>
                      New Project
                    </Button>

                  )}
                </Form.Item>

              </Col> */}
            </Row>
            <Row gutter={16}>
              {/* <Col span={12}>
                <Form.Item label="Reminder">
                  {getFieldDecorator('reminder', {
                    initialValue: editTaskData.reminder != null ? moment(editTaskData.reminder, 'YYYY-MM-DD HH:mm') : null,
                    rules: [{ required: false, message: 'Please choose the reminder' }],
                  })(
                    <DatePicker
                      disabled={disable}
                      showTime={{ format: 'HH:mm' }}
                      format="YYYY-MM-DD HH:mm"
                      placeholder="Select Time"
                      onChange={(value, dateString) => {
                        console.log('Selected Time: ', value);
                        console.log('Formatted Selected Time: ', dateString);
                      }}
                      onOk={(value) => {
                        console.log('onOk: ', value);
                      }}
                    />
                  )}
                </Form.Item>

              </Col> */}
              <Col span={12}>
                <Form.Item label="Tag">
                  {getFieldDecorator('tag', {
                    initialValue: editTaskData.tag,
                    rules: [{ required: false, message: 'Please choose the Tag' }],
                  })(
                    <Input placeholder="Please enter Tag" disabled={disable} />
                  )}
                </Form.Item>

              </Col>
            </Row>

          </Form>
          <Row gutter={16}>
            <Tabs defaultActiveKey="1" onChange={this.onChangeTabs}>
              <TabPane tab="Comment" key="1">
                <Row>
                  {this.showListComment(comments)}
                </Row>
                <Row>
                  <Col span={20}>
                    <Form  >
                      <Form.Item >
                        {getFieldDecorator('comment', {
                          rules: [],
                        })(
                          <Input placeholder="Add comment" />
                        )}
                      </Form.Item>
                    </Form>
                  </Col>
                  <Col span={4}>
                    <Button onClick={this.onNewCommentSubmit} type="primary" >
                      Comment
                    </Button>
                  </Col>

                </Row>
              </TabPane>
              <TabPane tab="History" key="2">
                {this.showHistory(histories)}
              </TabPane>

            </Tabs>
          </Row>
          <div
            style={{
              position: 'absolute',
              left: 0,
              bottom: 0,
              width: '100%',
              borderTop: '1px solid #e9e9e9',
              padding: '10px 16px',
              background: '#fff',
              textAlign: 'right',
            }}
          >
            <Button onClick={this.onClose} style={{ marginRight: 8 }}>
              Cancel
            </Button>
            <Button type="primary" onClick={() => this.setState({ disable: false })} style={{ marginRight: 8 }}>
              Edit
            </Button>
            <Button onClick={this.onEditTaskSubmit} type="primary" disabled={disable}>
              Save
            </Button>
          </div>
        </Drawer>
      </div>
    );
  }
}

//   const App = Form.create()(NewTask);
const mapStatetoProps = state => ({
  listUser: state.users.users,
  editTaskData: state.tasks.editTask,
  auth: state.auth.user,
  tasks: state.tasks
})
const mapDispatchtoProp = (dispatch, props) => {
  return {

  }
}
export default connect(mapStatetoProps, mapDispatchtoProp)(Form.create()(EditTask));