import React, { Component } from 'react';
import { Link } from "react-router-dom";
import moment from "moment";
import Highlighter from 'react-highlight-words';
import { Row, Col, Table, Tag, Button, Icon, Menu, Input } from 'antd';
import NewTask from './NewTask';
import EditTask from './EditTask';
import ListReminder from '../reminder/ListReminder';

//-----Table


class TaskList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      newTaskShow: false,
      editTaskShow: false,
      newReminderShow: false,
      listReminderShow: false,
      data: [],
      myProject: [],
      classMenu: null
    }
    // rowSelection object indicates the need for row selection
    this.rowSelection = {
      onChange: (selectedRowKeys, selectedRows) => {
        console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
      },
      getCheckboxProps: record => ({
        disabled: record.name === 'Disabled User', // Column configuration not to be checked
        name: record.name,
      }),
    };
  }
  expandedRowRender = (record) => {
    const columns = [
      {
        title: 'Supervisor Person',
        dataIndex: 'supervisorPerson',
        render: (text, record) => {
          return (
            <div>
               {record.supervisorPerson?record.supervisorPerson.name:''}
            </div>
          )
        }
      },
      {
        title: 'Description',
        dataIndex: 'description',
     
      },
      // {
      //   title: 'Reminder',
      //   dataIndex: 'reminder',
      //   width: "40%",
      //   render: (text, record) => {
      //     return (record.reminder != null ? moment(record.reminder).format('YYYY-MM-DD HH:mm') : 'Not reminder')
      //   }
      // },
      {
        title: 'Files',
        dataIndex: 'files',
        render: (text, record) => {
          return (
            <div>
              {record.files.map(fileitem => {
                return fileitem.fileinfor.map(item => {
                  return (
                    <div>
                      <Link
                        onClick={() => this.props.onDownloadFile(fileitem)}>
                        {item.name}
                      </Link>
                    </div>
                  )
                })
              })
              }
            </div>
          )
        }
      },
      {
        title: 'Tag',
        dataIndex: 'tag'
      },
    ];
    let data = [
      {
        supervisorPerson:record.supervisorPerson,
        description: record.description,
        reminder: record.reminder,
        tag: record.tag,
        files: record.files
      }
    ]

    return <Table columns={columns} dataSource={data} pagination={false} />;
  };
  showFilter = (users) => {
    users = users.map((user, index) => {
      return {
        text: user.name,
        value: user.name
      }
    })
    return users;
  }
  //onClick status
  onClickChangeStatus = (record, status) => {

    this.props.onEditStatus(record, status);
  }
  //filter and serach
  getColumnSearchProps = dataIndex => ({
    filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={node => {
            this.searchInput = node;
          }}
          placeholder={`Search ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
          onPressEnter={() => this.handleSearch(selectedKeys, confirm)}
          style={{ width: 188, marginBottom: 8, display: 'block' }}
        />
        <Button
          type="primary"
          onClick={() => this.handleSearch(selectedKeys, confirm)}
          icon="search"
          size="small"
          style={{ width: 90, marginRight: 8 }}
        >
          Search
            </Button>
        <Button onClick={() => this.handleReset(clearFilters)} size="small" style={{ width: 90 }}>
          Reset
            </Button>
      </div>
    ),
    filterIcon: filtered => (
      <Icon type="search" style={{ color: filtered ? '#1890ff' : undefined }} />
    ),
    onFilter: (value, record) =>
      record[dataIndex]
        .toString()
        .toLowerCase()
        .includes(value.toLowerCase()),
    onFilterDropdownVisibleChange: visible => {
      if (visible) {
        setTimeout(() => this.searchInput.select());
      }
    },
    render: text => (
      <Highlighter
        highlightStyle={{ backgroundColor: '#ffc069', padding: 0 }}
        searchWords={[this.state.searchText]}
        autoEscape
        textToHighlight={text.toString()}
      />
    ),
  });

  handleSearch = (selectedKeys, confirm) => {
    confirm();
    this.setState({ searchText: selectedKeys[0] });
  };

  handleReset = clearFilters => {
    clearFilters();
    this.setState({ searchText: '' });
  };
  //End filter and serach
  onClickEdit = (record) => {
    this.setState({
      editTaskShow: true
    })
    this.props.onGetEditTask(record);
  }
  componentWillReceiveProps(nextProps) {
    let data = [];
    let { tasks, MyProjects, users, reminder } = nextProps;
    if (tasks.length > 0) {
      tasks.forEach((task, index) => {
        data.push({
          key: index,
          name: task.name,
          status: /*moment(task.deadline).fromNow().indexOf("ago") > 0 ? "OVER" : */task.status,
          description: task.description,
          startOn: task.startOn,
          deadline: task.deadline,
          supervisorPerson: task.supervisorPerson,
          reponsiblePerson: task.reponsiblePerson.map((item, i) => {
            return item.name
          }).join(', '),
          reponsiblePersonId: task.reponsiblePerson.map((item, i) => {
            return item._id
          }),
          createByName: task.createBy ? task.createBy.name : '',
          createById: task.createBy ? task.createBy._id : '',
          createByEmail: task.createBy ? task.createBy.email : '',
          project: task.projectId == null ? '' : task.projectId.name,
          projectId: task.projectId == null ? '' : task.projectId._id,
          id: task._id,
          tag: task.tag,
          files: task.listFile
        })
      })
    }

    this.setState({
      data: [...data],
      myProject: MyProjects
    })
  }
  countTaskOver = (tasks) => {
    let result = null;
    result = tasks.filter((project, index) => {
      return moment(project.deadline).fromNow().indexOf("ago") > 0
    })
    return result.length;
  }
  showListProject = (listProject) => {
    let result = null;
    result = listProject.map((project, index) => {
      return (
        <Menu.Item onClick={() => this.onClickMenuProject(project._id)}
          id={project._id} key={index}
          className="ant-menu-item">
          <Icon type="mail" />{project.name}

        </Menu.Item>
      )
    })
    return result;
  }
  setStatusForShow = () => {
    this.setState({
      listReminderShow: false,
      newTaskShow: false
    })
  }
  render() {
    const { AllProjects, users } = this.props;
    const columns = [{
      title: 'Name',
      dataIndex: 'name',
      ...this.getColumnSearchProps('name'),
    }, {
      title: 'Status',
      dataIndex: 'status',
      render: (text, record) => {

        // if (moment(record.deadline).fromNow().indexOf("ago") > 0) { // so sánh này k để đây, k tự change status được. 
        //   return (
        //     <Tag color="#854511">{"OVER"}</Tag>
        //   )
        // } else 
        if (text === "DONE") {
          if (this.props.auth.sub === record.supervisorPerson._id) {
            return (
              <div>
                <Tag className="status" color="#854511" onClick={() => this.onClickChangeStatus(record, "START")}>{"START"}</Tag>
                <Tag className="status" color="#87d068" onClick={() => this.onClickChangeStatus(record, "FINISH")}>{"FINISH"}</Tag>
              </div>
            )
          } else {
            return (
              <div>
                <Tag className="status" color="#854511" onClick={() => this.onClickChangeStatus(record, "START")}>{"START"}</Tag>
                <Tag color="gray">{"FINISH"}</Tag>
              </div>
            )
          }
        }
        if (text == "FINISH") {
          return (
            <div>
              <Tag className="status" color="#87d068"
              // onClick={() => this.onClickChangeStatus(record, "FINISH")}
              >
                {text + "ED"}</Tag>
            </div>
          )
        } else if (text == "CONFIRM") {

          return (
            <Tag className="status" color="magenta" onClick={() => this.onClickChangeStatus(record, "START")} >{text}</Tag>
          )
        } else if (text == "START" || text == "PAUSE") {
          return (
            <div>
              <Tag className="status" color="#854511" onClick={() => this.onClickChangeStatus(record, text == "START" ? "PAUSE" : "START")}>{text == "START" ? "PAUSE" : "START"}</Tag>
              <Tag className="status" color="#87d068" onClick={() => this.onClickChangeStatus(record, "DONE")}>DONE</Tag>
            </div>
          )
        }
      },
      filters: [
        {
          text: 'CONFIRM',
          value: 'CONFIRM',
        },
        {
          text: 'START',
          value: 'START',
        },
        {
          text: 'PAUSE',
          value: 'PAUSE',
        },
        {
          text: 'DONE',
          value: 'DONE',
        },
        {
          text: 'FINISH',
          value: 'FINISH',
        },
        {
          text: 'OVER',
          value: 'OVER',
        }],
      onFilter: (value, record) => record.status.indexOf(value) === 0
    }, {
      title: 'Deadline',
      dataIndex: 'deadline',
      render: (text, record) => {
        return text != null ? moment(text).format('YYYY-MM-DD HH:mm') : 'Not deadline'
      }
    }, {
      title: 'Create By',
      dataIndex: 'createByName',
      render: (text, record) => {
        return (text)
      },
      filters: this.showFilter(this.props.users),
      onFilter: (value, record) => record.createByName.indexOf(value) === 0

    }, {
      title: 'Reponsible Person',
      dataIndex: 'reponsiblePerson',
      render: (text, record) => {
        return (text)
      },
      // filters:this.showFilter(this.props.users),
      // onFilter: (value, record) => record.reponsiblePerson.indexOf(value) === 0

    }, {
      title: 'Project',
      dataIndex: 'project',
      filters: this.showFilter(this.props.AllProjects),
      onFilter: (value, record) => record.project.indexOf(value) === 0
    }, {

    }, {
      title: 'Action',
      dataIndex: 'action',
      render: (text, record) => {
        return (<a href="javascript:;" onClick={() => this.onClickEdit(record)}><Icon type="edit" /></a>)
      }

    }];
    return (
      <div className="mybody">
        <Row type="flex">
          <Col span={12} className="col">
            <Button type="primary" className="my-btn" size="default" onClick={() => this.setState({ newTaskShow: true })}>New Task</Button>
            <Button type="primary" className="my-btn" size="default" onClick={() => this.setState({ listReminderShow: true })}>List Reminder</Button>
            <NewTask
              show={this.state.newTaskShow}
              listUser={users}
              AllProjects={AllProjects}
              onNewTask={this.props.onNewTask}
              setStatusForShow={this.setStatusForShow}
              user={this.props.user}//quên mất lm gì r
              auth={this.props.auth}

            />
            <EditTask
              show={this.state.editTaskShow}
              listUser={users}
              onEditTask={this.props.onEditTask}
              onNewComment={this.props.onNewComment}
              AllProjects={AllProjects}
              setStatusForShow={this.setStatusForShow}
            />

            <ListReminder
              showListReminder={this.state.listReminderShow}
              listTask={this.state.data}
              listReminder={this.props.reminder}
              onEditReminder={this.props.onEditReminder}
              onNewReminder={this.props.onNewReminder}
              setStatusForShow={this.setStatusForShow}
            />
          </Col>
        </Row>
        <Row style={{ marginTop: "10px" }}>
          <Col span={12}>
            <Tag color="#87d068">Task : </Tag><span>{this.state.data.length}</span>
            <Tag color="#f50">Overdue Task : </Tag><span>{this.countTaskOver(this.state.data)}</span>
          </Col>
          <Col span={12}>
            <Button type="primary" >List</Button>
            <Button type="success" >Planner</Button>
          </Col>
        </Row>
        {
          //TaskList Content
        }
        <Row>
          <Col span={24}>
            <Table
              rowSelection={null}
              rowClassName={(record, index) => {
                if (moment(record.deadline).fromNow().indexOf("ago") > 0) {
                  return "rowClassName-table-row"
                }
              }}
              columns={columns}
              expandedRowRender={(record) => {
                return this.expandedRowRender(record)
              }}
              dataSource={this.state.data} />
          </Col>
        </Row>
        {/* <TaskAction /> */}
      </div>
    );
  }
}
export default TaskList;
