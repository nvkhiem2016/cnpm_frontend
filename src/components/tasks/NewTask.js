import React, { Component } from 'react';
import { connect } from "react-redux";
import moment from "moment";
import {
  Drawer, Form, Button, Col, Row, Input, Select, DatePicker, Icon,
} from 'antd';
import NewProject from '../projects/NewProject';

const { Option } = Select;
const { RangePicker } = DatePicker;
class NewTask extends Component {
  constructor(props) {
    super(props);
    this.state = {
      newProjectShow: false,
      visible: false

    };
  }
  componentWillReceiveProps(nextProps) {
    this.setState({
      visible: nextProps.show
    });
  }
  onClose = () => {
    this.props.setStatusForShow();
    this.setState({
      visible: false,
      newProjectShow: false
    });
  };
  onNewTaskSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        let newTask = {
          name: values.name,
          description: values.description,
          startOn: moment(values.startOn).format('YYYY-MM-DD HH:mm'),
          deadline: moment(values.deadline).format('YYYY-MM-DD HH:mm'),
          tag: values.tag,
          reponsiblePerson: values.reponsiblePerson,
          supervisorPerson:values.supervisorPerson,
          createBy:values.createBy,
          projectId: values.projectId,
          createAt: moment().format('YYYY-MM-DD HH:mm')
        }
        this.props.onNewTask(newTask)
      }
    });
  };
  onChange = (e) => { //không cho NewProject display
    this.setState({

      newProjectShow: false
    });
  }

  showListUser = (listUser) => {
    listUser = listUser.map((user, index) => {
      return (
        <Option value={user._id} key={index}>{user.name}</Option>
      )
    })
    return listUser;
  }
  showListProject = (listProject) => {

    listProject = listProject.map((project, index) => {
      return (
        <Option value={project._id} key={index}>{project.name}</Option>

      )
    })
    return listProject;
  }

  render() {
    const { getFieldDecorator } = this.props.form;
    const { listUser } = this.props;
    const { visible } = this.state;
    return (
      <div>
        <Drawer
          title="New Task"
          width={720}
          onClose={this.onClose}
          visible={visible}
          destroyOnClose={true}
        >
          <Form layout="vertical" hideRequiredMark>
            <Row gutter={16}>
              <Col span={24}>
                <Form.Item label="Name">
                  {getFieldDecorator('name', {
                    rules: [{ required: true, message: 'Please enter new task' }],
                  })(<Input placeholder="Please enter user name" />)}
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={16}>
              <Col span={24}>
                <Form.Item label="Description">
                  {getFieldDecorator('description', {
                    rules: [
                      {
                        required: false,
                        message: 'please enter  description',
                      },
                    ],
                  })(<Input.TextArea rows={4} placeholder="please enter  description" />)}
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={16}>
              <Col span={12}>
              <Form.Item label="Start on">
                  {getFieldDecorator('startOn', {
                    rules: [{ required: true, message: 'Please choose the startOn' }],
                    initialValue: moment(Date.now()),                  
                  })(
                    <DatePicker
                      showTime={{ format: 'HH:mm' }}
                      format="YYYY-MM-DD HH:mm"
                      style={{ width: '100%' }}
                    />
                  )}
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item label="Deadline">
                  {getFieldDecorator('deadline', {
                    rules: [{ required: true, message: 'Please choose the dateTime' }],
                  })(
                    <DatePicker
                      showTime={{ format: 'HH:mm' }}
                      format="YYYY-MM-DD HH:mm"
                      style={{ width: '100%' }}


                    />
                  )}
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={16}>
              <Col span={12}>
                <Form.Item label="Reponsible person">
                  {getFieldDecorator('reponsiblePerson', {
                    rules: [{ required: true, message: 'Please choose the reponsible' }],
                  })(
                    <Select

                      //mode="multiple"

                      placeholder="Please choose the person">
                      {this.showListUser(listUser)}
                    </Select>
                  )}
                </Form.Item>
              </Col>
              <Col span={12}>
              <Form.Item label="Supervisor Person">
                  {getFieldDecorator('supervisorPerson', {
                    rules: [{ required: true, message: 'Please choose the supervisor' }],
                  })(
                    <Select

                      //mode="multiple"

                      placeholder="Please choose the person">
                      {this.showListUser(listUser)}
                    </Select>
                  )}
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={16}>
              <Col span={12}>
                <Form.Item label="Create By">
                  {getFieldDecorator('createBy', {
                    rules: [{ required: true, message: 'Please choose the reponsible' }],
                    initialValue:this.props.auth.sub
                  })(
                    <Select
                      onChange={this.onChange}
                      placeholder="Please choose the create by"
                    >
                      {this.showListUser(listUser)}
                    </Select>
                  )}
                </Form.Item>


              </Col>
              <Col span={12}>
                <Form.Item label="Project">
                  {getFieldDecorator('projectId', {
                    rules: [{ required: true, message: 'Please choose the project' }],

                  })(
                    <Select
                      onChange={this.onChange}
                      placeholder="Please choose the project"
                    >
                      <Option value={null} key={-1}>Null</Option>
                      {this.showListProject(this.props.AllProjects)}
                    </Select>
                  )}
                </Form.Item>


              </Col>
              <Col span={12}>
                {/* <Form.Item label="New project">
                  {getFieldDecorator('NewProjects', {
                    rules: [{ required: false, message: 'Please choose the project' }],
                  })(
                    <Button type="primary" onClick={() => this.setState({ newProjectShow: true })}>
                      New Project
                    </Button>
              
                  )}
                </Form.Item> */}
                {/* <NewProject 
                  show={this.state.newProjectShow}
                  user={this.props.user}
                /> */}
              </Col>
            </Row>
            {/* <Row gutter={16}>
              <Col span={12}>
                <Form.Item label="Reminder">
                  {getFieldDecorator('reminder', {
                    rules: [{ required: false, message: 'Please choose the reminder' }],
                  })(
                    <DatePicker
                      showTime
                      placeholder="Select Time"
                      onChange={(value, dateString) => {
                        console.log('Selected Time: ', value);
                        console.log('Formatted Selected Time: ', dateString);
                      }}
                      onOk={(value) => {
                        console.log('onOk: ', value);
                      }}
                    />
                  )}
                </Form.Item>
              </Col>
              <Col span={12}>
              <Form.Item label="Description Reminder">
                  {getFieldDecorator('descriptionReminder', {
                    rules: [
                      {
                        required: false,
                        message: 'please enter  description for reminder',
                      },
                    ],
                  })(<Input.TextArea rows={4} placeholder="please enter  description for reminder" />)}
                </Form.Item>
              </Col>
            </Row> */}
            <Row gutter={16}>
              <Col span={12}>
                <Form.Item label="Tag">
                  {getFieldDecorator('tag', {
                    rules: [{ required: false, message: 'Please choose the Tag' }],
                  })(
                    <Input placeholder="Please enter Tag" />
                  )}
                </Form.Item>

              </Col>
            </Row>
          </Form>
          <div
            style={{
              position: 'absolute',
              left: 0,
              bottom: 0,
              width: '100%',
              borderTop: '1px solid #e9e9e9',
              padding: '10px 16px',
              background: '#fff',
              textAlign: 'right',
            }}
          >
            <Button onClick={this.onClose} style={{ marginRight: 8 }}>
              Cancel
              </Button>
            <Button onClick={this.onNewTaskSubmit} type="primary">
              Submit
              </Button>
          </div>
        </Drawer>
      </div>
    );
  }
}

//   const App = Form.create()(NewTask);
const mapStatetoProps = state => ({
  //listUser : state.users.users
})
const mapDispatchtoProp = (dispatch, props) => {
  return {

  }
}
export default connect(mapStatetoProps, mapDispatchtoProp)(Form.create()(NewTask));