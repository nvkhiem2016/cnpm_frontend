import React, { Component } from 'react';
import { Row, Col, Button,Dropdown,Icon,Menu } from 'antd';


const menu = (
  <Menu onClick={""}>
    <Menu.Item key="1"><Icon type="user" />Set deadline</Menu.Item>
    <Menu.Item key="2"><Icon type="user" />Finish</Menu.Item>
    <Menu.Item key="3"><Icon type="user" />Remove</Menu.Item>
  </Menu>
);
class TaskAction extends Component {
  constructor(props) {
    super(props);
    this.state = {
    }
  }
  render() {
    return (
      <div>
        <Row>
          <Col span={24}>
            <Dropdown overlay={menu} trigger="click">
              <Button style={{ marginLeft: 8 }}>
                Select Action <Icon type="down" />
              </Button>
            </Dropdown>
            <Button type="primary" style={{ marginLeft: 8 }}>
              Apply
             </Button>
          </Col>
        </Row>
      </div>
    );
  }
}
export default TaskAction;
