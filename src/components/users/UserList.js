import React, { Component } from 'react';
import { Row, Col, Table, Tag, Button, Icon, Menu, Input, Select } from 'antd';
import NewUser from './NewUser';

//-----Table


class UserList extends Component {
  constructor(props) {
    super(props);
    this.state = {

    }
    this.columns = [
      {
        title: 'Name',
        dataIndex: 'name',
        key: 'name',
        width: "20%",
        render: (text, record) => {
          return (text);
        }
      },
      {
        title: 'Email',
        dataIndex: 'email',
        key: 'email',
        width: "20%",
        render: (text, record) => {
          return (text);
        }
      },
      {
        title: 'Address',
        dataIndex: 'address',
        key: 'address',
        render: (text, record) => {
          return (text);
        }
      },
    ];
  }
  setStatusForShow = () => {
    this.setState({
      newUserShow: false
    })
  }
  render() {
    return (
      <div className="mybody">
        <Row gutter={16}>
          <Col span={24}>
            <Button type="primary" size="default" onClick={() => this.setState({ newUserShow: true })} >New User</Button>
            <NewUser
              show={this.state.newUserShow}
              setStatusForShow={this.setStatusForShow}
              onNewUser={this.props.onNewUser}
            />
            <Table dataSource={this.props.users} columns={this.columns} />
          </Col>
        </Row>
      </div>
    );
  }
}
export default UserList;
