import React, { Component } from 'react';
import moment from "moment";
import {
  Drawer, Form, Button, Col, Row, Input, Select, DatePicker, Icon,
} from 'antd';
const { Option } = Select;
class NewUser extends Component {
  constructor(props) {
    super(props);
    this.state = {
      newProjectShow: false,
      visible: false

    };
  }
  componentWillReceiveProps(nextProps) {
    this.setState({
      visible: nextProps.show
    });
  }
  onClose = () => {
    this.props.setStatusForShow();
    this.setState({
      visible: false,

    });
  };
  onNewUserSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        let user = {
          name: values.name,
          email: values.email,
          password: values.password,
          gender: values.gender,
          address: values.address,
        }
        this.props.onNewUser(user);
      }
    });
  };


  render() {
    const { getFieldDecorator } = this.props.form;
    const { visible } = this.state;
    return (
      <div>
        <Drawer
          title="New User"
          width={720}
          onClose={this.onClose}
          visible={visible}
          destroyOnClose={true}
        >
          <Form layout="vertical" hideRequiredMark>
            <Row gutter={16}>
              <Col span={24}>
                <Form.Item label="Name">
                  {getFieldDecorator('name', {
                    rules: [{ required: true, message: 'Please enter new task' }],
                  })(<Input placeholder="Please enter user name" />)}
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={16}>
              <Col span={12}>
                <Form.Item label="Email">
                  {getFieldDecorator('email', {
                    rules: [
                      {
                        required: true,
                        message: 'please enter email',
                      },
                    ],
                  })(<Input placeholder="Please enter email" type="email" />)}
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item label="Password">
                  {getFieldDecorator('password', {
                    rules: [
                      {
                        required: true,
                        message: 'please enter password',
                      },
                    ],
                  })(<Input placeholder="Please enter password" />)}
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={16}>
              <Col span={12}>
                <Form.Item label="Gender">
                  {getFieldDecorator('gender', {
                    initialValue: "Male",
                    rules: [
                      {
                        required: true,
                        message: 'please enter gender',
                      },
                    ],
                  })(
                    <Select placeholder="Please choose the person">
                      <Option value={"Male"}>{"Male"}</Option>
                      <Option value={"Female"}>{"Female"}</Option>

                    </Select>
                  )}
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item label="Birthday">
                  {getFieldDecorator('birthday', {
                    initialValue: moment(Date.now()),
                    rules: [{ required: true, message: 'Please choose the birthday' }],
                  })(
                    <DatePicker
                      format="YYYY-MM-DD"
                      style={{ width: '100%' }}
                    />
                  )}
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={16}>
              <Col span={24}>
                <Form.Item label="Address">
                  {getFieldDecorator('address', {
                    rules: [
                      {
                        required: true,
                        message: 'please enter address',
                      },
                    ],
                  })(
                    <Input placeholder="Please enter address" />
                  )}
                </Form.Item>
              </Col>

            </Row>
          </Form>
          <div
            style={{
              position: 'absolute',
              left: 0,
              bottom: 0,
              width: '100%',
              borderTop: '1px solid #e9e9e9',
              padding: '10px 16px',
              background: '#fff',
              textAlign: 'right',
            }}
          >
            <Button onClick={this.onClose} style={{ marginRight: 8 }}>
              Cancel
              </Button>
            <Button onClick={this.onNewUserSubmit} type="primary">
              Submit
              </Button>
          </div>
        </Drawer>
      </div>
    );
  }
}


export default (Form.create()(NewUser));