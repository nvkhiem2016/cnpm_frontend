import React, { Component } from 'react';
import { connect } from "react-redux";
import moment from "moment";
import {
  Modal, Upload, Icon, Form, Button, message, Input, Select
} from 'antd';
const { Dragger } = Upload;
const { Option } = Select;

class UploadFile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      newProjectShow: false,
      visible: false,
      files: null
    };
  }
  handleOk = e => {
    this.props.setStatusForShow();
    this.setState({
      visible: false,
    });
  };

  handleCancel = e => {
    this.props.setStatusForShow();
    this.setState({
      visible: false,
    });
  };
  componentWillReceiveProps(nextProps) {
    if (nextProps.show) {
      this.setState({
        visible: nextProps.show
      });
    }
  }
  onUploadFiles = e => {
    e.preventDefault();
    let { files } = this.state;
    this.props.form.validateFields((err, values) => {
      if (!err) {
        let data = {
          files: files,
          name:values.name,
          idTask: values.idTask,
          idProject: this.props.idProject
        }
        console.log('onUploadFiles:', data);
        this.props.onNewFile(data)
      }
    });
  };
  onChangeFile = e => {
    let files = e.target.files;
    this.setState({
      files: files[0]
    })

  }
  showListTask = (listTask) => {
    listTask = listTask.map((task, index) => {
      return (
        <Option value={task._id} key={index} >{task.name}</Option>
      )
    })
    return listTask;
  }
  render() {
    const { getFieldDecorator } = this.props.form;
    const { tasks } = this.props;

    return (
      <div>
        <Modal
          title="Basic Modal"
          visible={this.state.visible}
          onOk={this.handleOk}
          onCancel={this.handleCancel}
        >
          <Form onSubmit={this.onUploadFiles}>
            <Form.Item label="Name">
              {getFieldDecorator('name', {
                rules: [{ required: false, message: 'Please enter new task' }],
              })(<Input placeholder="Please enter user name" />)}
            </Form.Item>
            <Form.Item label="Task">
              {getFieldDecorator('idTask', {
                rules: [{ required: false, message: 'Please choose the task' }],
              })(
                <Select
                  placeholder="Please choose the task">
                  <Option value={null} key={-1}>Null</Option>
                  {this.showListTask(tasks)}
                </Select>
              )}
            </Form.Item>
            <Form.Item label="Files">
              {getFieldDecorator('fileList', {
                rules: [{ required: true, message: 'Please choose file' }],

              })(
                <Input type="file" onChange={this.onChangeFile} multiple={false} />
              )}
            </Form.Item>
            <Form.Item>
              <Button type="primary" htmlType="submit">
                Submit
              </Button>
            </Form.Item>
          </Form>
        </Modal>
      </div>
    );
  }
}

//   const App = Form.create()(UploadFile);
const mapStatetoProps = state => ({
  //listUser : state.users.users
})
const mapDispatchtoProp = (dispatch, props) => {
  return {

  }
}

export default Form.create({ name: 'form_upload' })(UploadFile);