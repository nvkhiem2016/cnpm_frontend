import React, { Component } from 'react';
import moment from 'moment';
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import {
  Row, Col, Button, Table
} from 'antd';
import UploadFile from './UploadFile';

class FileList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      newUploadFileShow: false
    };
    this.columns = [
      {
        title: 'Task Name',
        dataIndex: 'idTask',
        render: (text, record) => {
          return `${record.idTask?record.idTask.name:"General"}`;
        }
      },
      {
        title: 'Create By',
        dataIndex: 'createBy',
        render: (text, record) => {
          return `${record.createBy.name}`;
        }
      },
      {
        title: 'File Name',
        dataIndex: 'fileinfor',
        render: (text, record) => {
          return (
            <div>
              {record.fileinfor.map(item => {
                return (
                  <Link
                    onClick={() => this.props.onDownloadFile(record)}>
                    {item.name}
                  </Link>
                )
              })
              }
            </div>
          )
        }
      },
      {
        title: 'Create At',
        dataIndex: 'createdAt',
        render: (text, record) => {
          return text != null ? moment(text).format('YYYY-MM-DD HH:mm') : 'Not Upload Date';
        }
      },
    ];

  }
  onClickUpload = () => {
    this.setState({
      newUploadFileShow: true
    })

  }
  setStatusForShow = () => {
    this.setState({
      newUploadFileShow: false
    })
  }
  render() {
    let { files } = this.props;
    return (
      <div>
        <Row gutter={16}>
          <Col span={24}>
            <Button type="primary" onClick={this.onClickUpload}>
              Upload File
            </Button>
            <UploadFile
              show={this.state.newUploadFileShow}
              setStatusForShow={this.setStatusForShow}
              idProject={this.props.idProject}
              onNewFile={this.props.onNewFile}
              tasks={this.props.tasks}
            />
          </Col>
        </Row>
        <Row gutter={16}>
          <Col span={24}>
            <Table dataSource={files} columns={this.columns} />

          </Col>
        </Row>
      </div>
    );
  }
}

const mapStatetoProps = state => ({
  //listUser : state.users.users
})
const mapDispatchtoProp = (dispatch, props) => {
  return {

  }
}
export default FileList;