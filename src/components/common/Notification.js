import {  notification } from 'antd';
const openNotification = (data) => {
    notification[data.type]({
      message: data.nameTask,
      description:
        data.description,
      onClick: () => {
        console.log('Notification Clicked!');
      },
      
    });
  };
export default openNotification;