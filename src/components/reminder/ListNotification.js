import React, { Component } from 'react';
import { Modal, List, Avatar, Row, Col } from 'antd';
import { connect } from 'react-redux';
import moment from "moment";
import { readReminderReq } from '../../actions/reminderAction';

class ListNotification extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: false,
      reminder:[]
    };
  }
  showModal = () => {
    this.setState({
      visible: true,
    });
  };

  handleOk = e => {
    this.props.setStatusForShow();
    this.setState({
      visible: false,
    });
  };

  handleCancel = e => {
    this.props.setStatusForShow();
    this.setState({
      visible: false,
    });
  };
  componentDidMount() {


  }
  componentWillReceiveProps(nextProps) {
    if(nextProps.show){
      this.setState({
        visible: nextProps.show
      });
    }
    if(nextProps.reminder){

      let {reminder} = this.state;
      reminder = nextProps.reminder.map((item,index)=>{
        return {
          ...item,
          index,
          isClassRead:false
        }
      })
      this.setState({
        reminder:reminder
      })
    }
  }
  showReminderUnRead = (reminder)=>{
    let result = null;
    result = reminder.filter(item=>{
      return item.isRead===false
    })
    return result;
  }
  onClickReadReminder=(item)=>{
    let {reminder} = this.state;
    
    this.setState({//set lai để làm mất item, lun r
      reminder:reminder
    })
    item.isRead = true;
    this.props.onClickReadReminder(item)
  }
  render() {
    let {reminder} = this.props;
    return (
      <div>
        <Modal
          title={"Notification"}
          visible={this.state.visible}
          onOk={this.handleOk}
          onCancel={this.handleCancel}
          width={"65%"}
          destroyOnClose={true}
        >
          <Row>
            <List
              
              itemLayout="horizontal"
              dataSource={this.showReminderUnRead(reminder)}
              renderItem={item => (
                <List.Item>
                  <List.Item.Meta
                    avatar={<Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />}
                    title={<a>{item.nameTask}</a>}
                    description={item.description}
                    style={{height:"45px"}}
                  />
                  <div><a style={{color:item.isClassRead?"red":"blue"}} onClick={(e)=>{

                    this.onClickReadReminder(item)
                    }}>Click to read</a></div>
                </List.Item>
                
              )}
            />,
          </Row>
        </Modal>
      </div>
    );
  }
}
const mapStateToProps = state => ({
  reminder: state.reminder.reminder
});
const mapDispatchtoProps = (dispatch, props) => {
  return {
    onClickReadReminder : (reminder)=>{
      dispatch(readReminderReq(reminder))
    }
  }
}
export default connect(mapStateToProps, mapDispatchtoProps)(ListNotification);