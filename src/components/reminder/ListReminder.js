import React, { Component } from 'react';
import { Modal, Button, Row, Col, Table, Divider, Select, DatePicker, Input } from 'antd';
import { connect } from 'react-redux';
import moment from "moment";
import NewReminder from './NewReminder';
const { Option } = Select;


class ListReminder extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: false,
      newReminderShow: false,
      listReminder: null
    };
  }
  showModal = () => {
    this.setState({
      visible: true,
    });
  };

  handleOk = e => {
    this.props.setStatusForShow();
    this.setState({
      visible: false,
    });
  };

  handleCancel = e => {
    this.props.setStatusForShow();
    this.setState({
      visible: false,
    });
  };
  componentDidMount() {


  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.showListReminder) {
      this.setState({
        visible: nextProps.showListReminder
      })
    }

  }
  showListTask = (listTask) => {
    listTask = listTask.map((task, index) => {
      return (
        <Option value={task.id} key={index}>{task.name}</Option>
      )

    })
    return listTask;
  }
  onEditReminder = (reminder) => {
    this.props.onEditReminder(reminder);
  }
  setStatusForShow = () => {
    this.setState({
      newReminderShow: false
    })
  }

  render() {
    let { listTask, listReminder } = this.props;
    let { } = this.state;
    const columns = [
      {
        title: 'idTask',
        dataIndex: 'idTask',
        key: 'idTask',
        render: (text, record) => {
          return (
            <Select
              defaultValue={text}
              placeholder="Please choose the person"
              onChange={(value) => {
                record.idTask = value;
              }}>
              <Option value={null} key={-1}>Null</Option>
              {this.showListTask(listTask)}
            </Select>
          )
        }
      },
      {
        title: 'description',
        dataIndex: 'description',
        key: 'description',
        render: (text, record) => {
          return (
            <Input.TextArea
              rows={1}
              defaultValue={text}
              onChange={(e) => {
                record.description = e.target.value;
              }}
            />
          )
        }
      },
      {
        title: 'reminderAt',
        dataIndex: 'reminderAt',
        key: 'reminderAt',
        render: (text, record) => {
          return (
            <DatePicker
              defaultValue={moment(record.reminderAt)}
              showTime={{ format: 'HH:mm' }}
              format="YYYY-MM-DD HH:mm"
              style={{ width: '100%' }}
              onChange={(value) => {
                console.log(moment(value).format('YYYY-MM-DD HH:mm'))
                record.reminderAt = moment(value).format('YYYY-MM-DD HH:mm')
              }}
            />)
        }
      },
      {
        title: 'isRead',
        dataIndex: 'isRead',
        key: 'isRead',
        render: (text, record) => {
          return (text == true ? "Read" : "Unread")
        }
      },
      {
        title: 'Action',
        key: 'action',
        render: (text, record) => (
          <span>
            <a href="javascript:;" onClick={() => this.onEditReminder(record)}>Sửa</a>
            <Divider type="vertical" />
            <a href="javascript:;">Xóa</a>
          </span>
        ),
      },
    ];
    return (
      <div>
        <Modal
          title={"List Reminder"}
          visible={this.state.visible}
          onOk={this.handleOk}
          onCancel={this.handleCancel}
          width={"80%"}
          destroyOnClose={true}
        >
          <Row>
            <Button type="primary" className="my-btn" size="default" onClick={() => this.setState({ newReminderShow: true })}>New Reminder</Button>
            <NewReminder
              show={this.state.newReminderShow}
              listTask={this.props.listTask}
              onNewReminder={this.props.onNewReminder}
              setStatusForShow={this.setStatusForShow}
            />
            <Table columns={columns} dataSource={listReminder} />
          </Row>
        </Modal>
      </div>
    );
  }
}
const mapStateToProps = state => ({
  reminder: state.reminder
});
const mapDispatchtoProps = (dispatch, props) => {
  return {

  }
}
export default connect(mapStateToProps, mapDispatchtoProps)(ListReminder);