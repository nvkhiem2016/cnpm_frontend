import React, { Component } from 'react';
import { connect } from "react-redux";
import moment from "moment";
import {
  Drawer, Form, Button, Col, Row, Input, Select, DatePicker, Icon,
} from 'antd';


const { Option } = Select;
const { RangePicker } = DatePicker;
class NewReminder extends Component {
  constructor(props) {
    super(props);
    this.state = {
      newReminderShow: false,
      visible: false

    };
  }
  componentWillReceiveProps(nextProps) {
    this.setState({
      visible: nextProps.show
    });
  }
  onClose = () => {
    this.props.setStatusForShow();
    this.setState({
      visible: false,
      newReminderShow: false
    });
  };
  onNewReminderSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        let newReminder = {
          reminderAt:moment(values.reminderAt).format('YYYY-MM-DD HH:mm'),
          description:values.description,
          idTask:values.task
        }
        this.props.onNewReminder(newReminder)
      }
    });
  };
  onChange = (e) => { //không cho NewProject display
    console.log(`selected ${e}`);
    this.setState({
      newReminderShow: false
    });
  }

  showListTask = (listTask) => {
    listTask = listTask.map((task, index) => {
      return (
        <Option value={task.id} key={index} >{task.name}</Option>
      )
    })
    return listTask;
  }
  render() {
    const { getFieldDecorator } = this.props.form;
    const { listTask } = this.props;
    const { visible } = this.state;
    return (
      <div>
        <Drawer
          title="New Reminder"
          width={720}
          onClose={this.onClose}
          visible={visible}
          destroyOnClose={true}
        >
          <Form layout="vertical" hideRequiredMark>
            <Row gutter={16}>
              <Col span={12}>
                <Form.Item label="Task">
                  {getFieldDecorator('task', {
                    rules: [{ required: false, message: 'Please choose the task' }],
                  })(
                    <Select
                      placeholder="Please choose the task">
                      <Option value={null} key={-1}>Null</Option>
                      {this.showListTask(listTask)}
                    </Select>
                  )}
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item label="ReminderAt">
                  {getFieldDecorator('reminderAt', {
                    rules: [{ required: true, message: 'Please choose the datetime' }],
                  })(
                    <DatePicker
                      showTime={{ format: 'HH:mm' }}
                      format="YYYY-MM-DD HH:mm"
                      style={{ width: '100%' }}
                    />
                  )}
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={16}>
              <Col span={24}>
                <Form.Item label="Description">
                  {getFieldDecorator('description', {
                    rules: [
                      {
                        required: false,
                        message: 'please enter  description',
                      },
                    ],
                  })(<Input.TextArea rows={4} placeholder="please enter  description" />)}
                </Form.Item>
              </Col>
            </Row>
          </Form>
          <div
            style={{
              position: 'absolute',
              left: 0,
              bottom: 0,
              width: '100%',
              borderTop: '1px solid #e9e9e9',
              padding: '10px 16px',
              background: '#fff',
              textAlign: 'right',
            }}
          >
            <Button onClick={this.onClose} style={{ marginRight: 8 }}>
              Cancel
              </Button>
            <Button onClick={this.onNewReminderSubmit} type="primary">
              Submit
              </Button>
          </div>
        </Drawer>
      </div>
    );
  }
}


const mapStatetoProps = state => ({

})
const mapDispatchtoProp = (dispatch, props) => {
  return {

  }
}
export default connect(mapStatetoProps, mapDispatchtoProp)(Form.create()(NewReminder));