import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import moment from 'moment';
import { Card, Col, Row } from 'antd';
class ProjectList extends Component {

  showListProject = (projects) => {
    let result = null;
    result = projects.map((project, index) => {
      return (
        <Col span={8} className="card-project" key={index} style={{margin:"5px 0px"}}>
          <Card title={project.name} bordered={false} extra={<Link to={`projects/${project._id}`}>More</Link>}>{
            <div>
              <p>
                Description : {project.description}
              </p>
              <p>
                Start : {project.timeStart != null ? moment(project.timeStart).format('YYYY-MM-DD HH:mm') : 'Not Start'}
              </p>
              <p>
                End : {project.timeEnd != null ? moment(project.timeEnd).format('YYYY-MM-DD HH:mm') : 'Not End'}
              </p>
              <p>
                Member : {project.listUser?project.listUser.length:''}
              </p>
            </div>
          }</Card>
        </Col>
      )
    })
    return result;
  }
  render() {
    let { projects } = this.props
    return (
      <Row gutter={16}>
        {this.showListProject(projects)}

      </Row>
    );
  }
}

export default ProjectList
