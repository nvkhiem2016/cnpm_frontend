import React, { Component } from 'react';
import moment from "moment";

import {
  Drawer, Form, Button, Col, Row, Input, Select, DatePicker, Icon, Checkbox
} from 'antd';

const { Option } = Select;
const { RangePicker } = DatePicker;
class NewProject extends Component {
  state = { visible: false };

  showDrawer = () => {
    this.setState({
      visible: true,
    });
  };
  componentWillReceiveProps(nextProps) {
    this.setState({
      visible: nextProps.show
    });
  }
  onClose = () => {
    this.props.setStatusForShow();
    this.setState({
      visible: false
    });
  };
  showListUser = (listUser) => {
    listUser = listUser.map((user, index) => {
      return (
        <Option value={user._id} key={index}>{user.name}</Option>
      )
    })
    return listUser;
  }
  onNewProject = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        let project = {
          name: values.name,
          description: values.description,
          timeStart: moment(values.timeStart).format('YYYY-MM-DD HH:mm'),
          timeEnd: moment(values.timeEnd).format('YYYY-MM-DD HH:mm'),
          tag: values.tag,
          listUser: values.listUser,
          isPrivate: values.isPrivate
        }
        this.props.onNewProject(project)

      }
    });
    //console.log()
  }
  render() {
    const { getFieldDecorator } = this.props.form;
    let { listUser,user } = this.props;
    return (
      <div>
        <Drawer
          title="New Project"
          width={720}
          onClose={this.onClose}
          visible={this.state.visible}
          destroyOnClose={true}
        >

          <Form layout="vertical" hideRequiredMark>
            <Row gutter={16}>
              <Col span={12}>
                <Form.Item label="New project">
                  {getFieldDecorator('name', {
                    rules: [{ required: true, message: 'Please enter new Project' }],
                  })(<Input placeholder="Please enter project" />)}
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item label="isPrivate">
                  {getFieldDecorator('isPrivate', {

                  })(
                    <Checkbox >Private</Checkbox>
                  )}
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={16}>
              <Col span={24}>
                <Form.Item label="Description">
                  {getFieldDecorator('description', {
                    rules: [
                      {
                        required: true,
                        message: 'please enter  description',
                      },
                    ],
                  })(<Input.TextArea rows={4} placeholder="please enter  description" />)}
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={16}>
              <Col span={12}>
                <Form.Item label="TimeStart">
                  {getFieldDecorator('timeStart', {
                    rules: [{ required: true, message: 'Please choose the time start' }],
                  })(
                    <DatePicker
                      showTime={{ format: 'HH:mm' }}
                      format="YYYY-MM-DD HH:mm"
                      placeholder="Select Time"

                    />
                  )}
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item label="TimeEnd">
                  {getFieldDecorator('timeEnd', {

                    rules: [{ required: true, message: 'Please choose the time end' }],
                  })(
                    <DatePicker
                      showTime={{ format: 'HH:mm' }}
                      format="YYYY-MM-DD HH:mm"
                      placeholder="Select Time"

                    />
                  )}
                </Form.Item>
              </Col>

            </Row>
            <Row gutter={16}>
              <Col span={12}>
                <Form.Item label="List User">
                  {getFieldDecorator('listUser', {
                    initialValue:[user.sub],
                    rules: [{ required: true, message: 'Please choose the members' }],
                  })(
                    <Select
                      mode="multiple"
                      placeholder="Please choose the approver">
                      {this.showListUser(listUser)}
                    </Select>
                  )}
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={16}>
              <Col span={12}>
                <Form.Item label="Tag">
                  {getFieldDecorator('tag', {
                    rules: [{ required: false, message: 'Please choose the Tag' }],
                  })(
                    <Input placeholder="Please enter Tag" />
                  )}
                </Form.Item>

              </Col>
            </Row>
          </Form>
          <div
            style={{
              position: 'absolute',
              left: 0,
              bottom: 0,
              width: '100%',
              borderTop: '1px solid #e9e9e9',
              padding: '10px 16px',
              background: '#fff',
              textAlign: 'right',
            }}
          >
            <Button onClick={this.onClose} style={{ marginRight: 8 }}>
              Cancel
              </Button>
            <Button onClick={this.onNewProject} type="primary">
              Submit
              </Button>
          </div>
        </Drawer>
      </div>
    );
  }
}

//   const App = Form.create()(NewTask);
export default Form.create()(NewProject);