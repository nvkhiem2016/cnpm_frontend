import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import moment from 'moment';
import { Card, Col, Row } from 'antd';
import TaskProject from './TaskProject';
import OwnerProject from './OwnerProject';
class ContainerProject extends Component {
  render() {
    return (
      <Row gutter={16}>
        <Col span={18}>
          <TaskProject
            tasks={this.props.tasks}
            users={this.props.users}
            AllProjects={this.props.AllProjects}
            MyProjects={this.props.MyProject}
            user={this.props.user}//lm gì quên mất r
            auth={this.props.auth}
            onGetEditTask={this.props.onGetEditTask}
            onNewTask={this.props.onNewTask}
            onEditTask={this.props.onEditTask}
            onEditStatus={this.props.onEditStatus}
            onNewComment={this.props.onNewComment}
          />
        </Col>
        <Col span={6}>
          <OwnerProject
            project={this.props.project}
            listUser={this.props.listUser}
            onAddUser={this.props.onAddUser} />
        </Col>
      </Row>
    );
  }
}

export default ContainerProject;
