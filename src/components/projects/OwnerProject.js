import React, { Component } from 'react';
import { Spin, Row, Col, Layout, Card, Avatar, Popover, Select, Form, Button } from 'antd';
import { connect } from 'react-redux';
import moment from 'moment'
import {  } from '../../actions/projectAction';
const styleSpin = {
  'text-align': 'center',
  'background': 'rgba(0, 0, 0, 0.05)',
  'border-radius': '4px',
  'margin-bottom': '20px',
  'padding': '30px 50px',
  'margin': '20px 0',
}
const { Option } = Select;
class OwnerProject extends Component {
  constructor(props) {
    super(props);
    this.state = {
      addUser: []
    }
  }
  componentWillMount() {

  }
  componentWillReceiveProps(nextProps) {
    console.log(nextProps)
  }
  componentDidMount() {
    let {project} = this.props; 
    this.setState({
      project:project,
      addUser: project.listUser.map(item => {
        return item._id
      })
    })
  }
  showListUser = (listUser) => {
    listUser = listUser.map((user, index) => {
      return (
        <Option value={user._id} key={index} name={user.name} onChange={value => {
          console.log(value)
        }}>{user.name}</Option>
      )
    })
    return listUser;
  }

  onAddUser = (e) => {
    e.preventDefault();
    let { addUser } = this.state;
    let {project} = this.props;
    this.props.onAddUser(project, addUser)

  }
  render() {

    let { project,listUser  } = this.props;
    let { addUser } = this.state;
    if (Object.keys(project).length > 0) {
      const FormPop = (
        <Form layout="vertical">
          <Form.Item>

            <Select
              mode="multiple"
              defaultValue={addUser}
              onChange={value => {
                this.setState({
                  addUser: value
                })
              }}
              placeholder="Please choose the approver">
              {this.showListUser(listUser)}
            </Select>
          </Form.Item>
          <Form.Item>
            <Button onClick={this.onAddUser} type="primary">
              Save
              </Button>
          </Form.Item>
        </Form>
      )
      return (
        <Col span={6} >
          <Card title="Owner" bordered={false} style={{ width: 300 }}>
            <Row>
              <Col>
                <Avatar icon="user" /><span> {project.createBy.name}</span>
              </Col>
            </Row>
            <hr></hr>
            <Row>
              <Col>
                <p>CreateAt : {moment(project.timeStart).format('YYYY-MM-DD HH:mm')}</p>
                <p>TimeEnd : {moment(project.timeEnd).format('YYYY-MM-DD HH:mm')}</p>
              </Col>
            </Row>
            <hr></hr>
            <Row>

              <Col span={8}>
                <p>Members({project.listUser.length})</p>
              </Col>
              <Col span={8} offset={8}>

                <Popover placement="left"
                  title={"Add member"}
                  content={FormPop}
                  trigger="click"
                  style={{ width: '200px' }}
                  overlayStyle={{
                    width: "25vw"
                  }}
                >
                  <a>Add Member </a>
                </Popover>
              </Col>
            </Row>
            <Row>
              <Col>
                {project.listUser.map((item) => {
                  return (<Avatar
                    icon="user"
                    id={item._id}
                    title={item.name}
                    style={{
                      marginLeft: "5px",
                      cursor: "pointer"
                    }}
                  />)
                })}
              </Col>
            </Row>
          </Card>

        </Col>



      );
    }
  }
}
const mapStateToProps = state => ({
  

});
const mapDispatchtoProps = (dispatch, props) => {
  return {
    
  }
}
export default connect(mapStateToProps, mapDispatchtoProps)(OwnerProject);

