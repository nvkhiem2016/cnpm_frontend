import React from 'react';
import TaskPage from './pages/TaskPage/TaskPage'
import NotFoundPage from './pages/NotFoundPage/NotFoundPage'
import MyCalendar from './pages/MyCalendar/MyCalendar';
import ProjectPage from './pages/ProjectPage/ProjectPage';
import AuthPage from './pages/AuthPage/AuthPage';
import ProjectItemPage from './pages/ProjectPage/ProjectItemPage';
import ChatPage from './pages/ChatPage/ChatPage';
import UserPage from './pages/UserPage/UserPage';


//cấu hình đường dẫn đến các page
const routes = [
    {
        path:'/tasks',
        exact:true,
        main:()=><TaskPage />
    },
    {
        path:'/projects/:id',
        exact:true,
        main:({match,history})=><ProjectItemPage match={match} history={history}/>
    },
    {
        path:'/projects',
        exact:true,
        main:()=><ProjectPage />
    },
    {
        path:'/calendar',//sửa sản phẩm
        exact:true,
        main:({match,history})=><MyCalendar match={match} history={history}/> // match:Object{url,params,...}}
    },
    {
        path:'/chat',//sửa sản phẩm
        exact:true,
        main:({match,history})=><ChatPage match={match} history={history}/> // match:Object{url,params,...}}
    },
    {
        path:'/users',//sửa sản phẩm
        exact:true,
        main:({match,history})=><UserPage match={match} history={history}/> // match:Object{url,params,...}}
    },
    {
        path:'*',
        exact:true,
        main:({location})=><NotFoundPage location/>
    }
    
]

export default routes;