import callApi from './../utils/callApi'
import {
  CLEAR_CURRENT_PROFILE,GET_PROFILE
} from './../constants/types';
import { API_GET_PROFILE } from '../constants/Config';

 
export const clearCurrentProfile = () => {
  return {
    type: CLEAR_CURRENT_PROFILE

  }
}
export const getCurrentProfile = () => dispatch => {
  callApi(API_GET_PROFILE,'GET')
    .then(res => {
      console.log(res.data)
      dispatch({
        type: GET_PROFILE,
        payload: res.data
      })
    })
    .catch(err => {
      dispatch({
        type: GET_PROFILE,
        payload: {}
      })
    });
};