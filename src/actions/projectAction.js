import callApi from "./../utils/callApi";
import {fetchTasks} from './taskAction';
import { GET_MY_PROJECT,GET_ALL_PROJECT, NEW_PROJECT, ADD_USER_PROJECT, GET_TASKS_PROJECT } from "./../constants/types";
import { API_GET_MY_PROJECT, API_GET_ALL_PROJECT, API_POST_PROJECT, API_ADD_USER_PROJECT, API_GET_TASK_PROJECT } from "./../constants/Config";
import openNotification from "../components/common/Notification";
import { project_message } from "../constants/message";
export const fetchMyProjectReq = () => {
  return (dispatch) => {
    return callApi(API_GET_MY_PROJECT, 'GET', null).then(res => {
      dispatch(fetchMyProject(res.data.result))
    }).catch(err=>{
      console.log(err)
    })
  }
}
export const fetchMyProject = (myProject) => {
  return {
    type: GET_MY_PROJECT,
    payload: myProject
  }
}
export const fetchAllProjectReq = () => {
  return (dispatch) => {
    return callApi(API_GET_ALL_PROJECT, 'GET', null).then(res => {
      dispatch(fetchAllProject(res.data.result))
    }).catch(err=>{
      console.log(err)
    })
  }
}
export const fetchAllProject = (projects) => {
  return {
    type: GET_ALL_PROJECT,
    payload: projects
  }
}
export const newProjectReq = (project) => {
  return (dispatch) => {
    return callApi(API_POST_PROJECT, 'POST', project).then(res => {
      openNotification({type:"success",
      nameTask:project.name,
      description:project_message.NEW_PROJECT_SUCCESSFULLY})
      dispatch(newProject(res.data.result))
    }).catch(err=>{
      console.log(err)
    })
  }
}
export const newProject = (project) => {
  return {
    type: NEW_PROJECT,
    payload: project
  }
}

export const addUserProjectReq = (project,listUser) => {
  return (dispatch) => {
    return callApi(`${API_ADD_USER_PROJECT}/${project._id}`, 'PUT', {listUser}).then(res => {
      openNotification({type:"success",
      nameTask:project.name,
      description:project_message.ADD_USER_SUCCESSFULLY})
      dispatch(addUserProject(res.data.result))
    }).catch(err=>{
      console.log(err)
    })
  }
}
export const addUserProject = (project) => {
  return {
    type: ADD_USER_PROJECT,
    payload: project
  }
}

export const fetchTaskByProjectReq = (id) => {
  return (dispatch) => {
    return callApi(`${API_GET_TASK_PROJECT}/${id}`, 'GET', null).then(res => {
      dispatch(fetchTasks(res.data.result))
    }).catch(err=>{
      console.log(err)
    })
  }
}
// export const fetchTaskByProject = (projects) => {
//   return {
//     type: GET_TASKS_PROJECT,
//     payload: projects
//   }
// }