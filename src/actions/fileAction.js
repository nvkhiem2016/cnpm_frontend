import fileDownload from 'js-file-download';
import callApi from "./../utils/callApi";
import { NEW_FILE, GET_FILES } from "./../constants/types";
import { API_NEW_FILE,API_GET_FILE, API_DOWNLOAD_FILE } from "./../constants/Config";
import moment from 'moment'


export const fetchFileByProjectReq = (id) => {
  return (dispatch) => {
    return callApi(`${API_GET_FILE}/${id}`, 'GET', null).then(res => {
      dispatch(fetchFile(res.data.result))
    }).catch(err => {
      console.log(err)
    })
  }
}
export const fetchFile = (files) => {
  return {
    type: GET_FILES,
    payload: files 
  }
}

export const newFileReq = (data) => {
  return (dispatch) => {
    const form = new FormData() 
    form.append('files', data.files)
    form.append('name', data.name)
    form.append('idTask', data.idTask)
    form.append('idProject', data.idProject)

    return callApi(API_NEW_FILE, 'POST', form).then(res => {
      dispatch(newFile(res.data.result))
    }).catch(err => {
      console.log(err)
    })
  }
}
export const newFile = (file) => {
  return {
    type: NEW_FILE,
    payload: file
  }
}
export const downloadFileReq = (data) => {
  return (dispatch) => {
    return callApi(`${API_DOWNLOAD_FILE}/${data._id}`, 'GET', null).then(res => {
      console.log(res.data)
      fileDownload(res.data,data.fileinfor[0].name)
      //dispatch(fetchFile(res.data))
    }).catch(err => {
      console.log(err)
    })
  }
}
