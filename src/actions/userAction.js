import callApi from './../utils/callApi'
import { GET_USERS, ADD_USER_ONLINE, REMOVE_USER_ONLINE, CREATE_USER } from "./../constants/types";
import { API_GET_USERS,API_CREATE_USER } from "./../constants/Config";
import openNotification from "../components/common/Notification";
import { user_message } from '../constants/message';

export const fetchUsersReq = () => {
  return (dispatch) => {
    return callApi(API_GET_USERS, 'GET', null).then(res => {
      dispatch(fetchUsers(res.data.result))
    }).catch(err => {
      console.log(err)
    })
  }
}
export const fetchUsers = (users) => {
  return {
    type: GET_USERS,
    payload: users
  }
}

export const createUserReq = (user) => {
  console.log(user)
  return (dispatch) => {
    return callApi(API_CREATE_USER, 'POST', user).then(res => {
      openNotification({type:"success",
      nameTask:user.name,
      description:user_message.CREATE_USER_SUCCESSFULLY})
      dispatch(createUser(res.data.result))
    }).catch(err => {
      console.log(err)
    })
  }
}
export const createUser = (user) => {
  return {
    type: CREATE_USER,
    payload: user
  }
}


export const addUserOnline = (user) => {
  return {
    type: ADD_USER_ONLINE,
    payload: user
  }
}
export const removeUserOnline = (user) => {
  return {
    type: REMOVE_USER_ONLINE,
    payload: user
  }
}