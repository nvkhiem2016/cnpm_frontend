import callApi from "./../utils/callApi";
import { NEW_HISTORY,GET_ALL_HISTORY } from "./../constants/types";
import { API_NEW_HISTORY, API_GET_HISTORY } from "./../constants/Config";
import moment from 'moment'


export const fetchAllHistoryReq = (idTask) => {
  return (dispatch) => {
    return callApi(`${API_GET_HISTORY}/${idTask}`, 'GET', null).then(res => {
      dispatch(fetchHistory(res.data.result))
    }).catch(err => {
      console.log(err)
    })
  }
}
export const fetchHistory = (histories) => {
  return {
    type: GET_ALL_HISTORY,
    payload: histories
  }
}
export const newHistoryReq = (history) => {
  let createAt = moment().format('YYYY-MM-DD HH:mm');//dùng để lưu lịch sử
  let data = {
    taskId:history.id,
    newData:history.newData,
    oldData:history.oldData,
    updateDisposition:history.updateDisposition,
    createAt:createAt,
  }
  return (dispatch) => {
    return callApi(API_NEW_HISTORY, 'POST', data).then(res => {
      dispatch(newHistory(res.data.result))
    }).catch(err => {
      console.log(err)
    })
  }
}
export const newHistory = (history) => {
  return {
    type: NEW_HISTORY,
    payload: history
  }
}
