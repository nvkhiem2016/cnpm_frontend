import callApi from "./../utils/callApi";
import { NEW_COMMENT,GET_ALL_COMMENT } from "./../constants/types";
import { API_NEW_COMMENT, API_GET_COMMENT } from "./../constants/Config";


export const fetchAllCommentReq = (idTask) => {
  return (dispatch) => {
    return callApi(`${API_GET_COMMENT}/${idTask}`, 'GET', null).then(res => {
      dispatch(fetchComment(res.data.result))
    }).catch(err => {
      console.log(err)
    })
  }
}
export const fetchComment = (comments) => {
  return {
    type: GET_ALL_COMMENT,
    payload: comments
  }
}
export const newCommentReq = (comment) => {
  return (dispatch) => {
    return callApi(API_NEW_COMMENT, 'POST', comment).then(res => {
      dispatch(newComment(res.data.result))
    }).catch(err => {
      console.log(err)
    })
  }
}
export const newComment = (comment) => {
  return {
    type: NEW_COMMENT,
    payload: comment
  }
}
