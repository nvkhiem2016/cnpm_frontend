import callApi from "./../utils/callApi";
import { SET_REMINDER, PUSH_REMINDER, EDIT_REMINDER, READ_REMINDER } from "./../constants/types";
import { API_GET_REMINDER, API_POST_REMINDER, API_PUT_REMINDER, API_READ_REMINDER } from "./../constants/Config";
import openNotification from "../components/common/Notification";
import { reminder_message } from "../constants/message";




export const fetchReminderReq = () => {
  return (dispatch) => {
    return callApi(API_GET_REMINDER, 'GET', null).then(res => {
      dispatch(setReminder(res.data.result))
    }).catch(err => {
      console.log(err)
    })
  }
}
export const setReminder = (reminders) => {
  return {
    type: SET_REMINDER,
    payload: reminders
  }
}

export const newReminderReq = (reminder) => {
  return (dispatch) => {

    return callApi(API_POST_REMINDER, 'POST', reminder).then(res => {
      openNotification({
        type: "success",
        nameTask: res.data.result.idTask.name,
        description: reminder_message.NEW_REMINDER_SUCCESSFULLY
      })
      dispatch(pushReminder(res.data.result))
    }).catch(err => {
      console.log(err)
    })
  }
}
export const pushReminder = (reminder) => {
  return {
    type: PUSH_REMINDER,
    payload: reminder
  }
}
export const editReminderReq = (reminder) => {
  return (dispatch) => {
    console.log(reminder._id)
    return callApi(`${API_PUT_REMINDER}/${reminder._id}`, 'PUT', reminder).then(res => {
      openNotification({
        type: "success",
        nameTask: res.data.result.idTask.name,
        description: reminder_message.EDIT_REMINDER_SUCCESSFULLY
      })
      dispatch(editReminder(res.data.result))
    }).catch(err => {
      console.log(err)
    })
  }
}
export const editReminder = (reminder) => {
  return {
    type: EDIT_REMINDER,
    payload: reminder
  }
}

export const readReminderReq = (reminder) => {
  return (dispatch) => {

    return callApi(`${API_READ_REMINDER}/${reminder._id}`, 'PUT', reminder).then(res => {
      dispatch(readReminder(reminder))
    }).catch(err => {
      console.log(err)
    })
  }
}
export const readReminder = (reminder) => {
  return {
    type: READ_REMINDER,
    payload: reminder
  }
}