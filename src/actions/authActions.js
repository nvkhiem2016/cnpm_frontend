import axios from 'axios';
import setAuthToken from './../utils/setAuthToken';
import jwt_decode from 'jwt-decode';
import {API_URL} from './../constants/Config'
import { GET_ERRORS,SET_CURRENT_USER } from './../constants/types';
import { clearCurrentProfile,getCurrentProfile } from './profileAction';



// Login - Get User Token
export const loginUser = userData => dispatch => {
  axios
    .post(`${API_URL}/user/login`, userData)
    .then(res => {
      // Save to localStorage
      const token = res.data.result;
      // Set token to ls
      localStorage.setItem('jwtToken', token);
      // Set token to Auth header
      setAuthToken(token);
      // Decode token to get user data
      const decoded = jwt_decode(token);
      // Set current user
      dispatch(setCurrentUser(decoded));
      //dispatch(getCurrentProfile());
      //window.location.href="/tasks"
    })
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};
// Set logged in user
export const setCurrentUser = decoded => {
  return {
    type: SET_CURRENT_USER,
    payload: decoded
  };
};

// Log user out
export const logoutUser = () => {
  localStorage.removeItem('jwtToken');
  // Remove auth header for future requests
  setAuthToken(false);
  return (dispatch)=>{
    dispatch(setCurrentUser())
    dispatch(clearCurrentProfile())
  }
  // Remove token from localStorage


  
};
