import callApi from './../utils/callApi'
import { SET_LIST_TASK, SET_DISPLAY_TASK, EDIT_LIST_TASK } from "./../constants/types";


export const setListTask = (data) => {
    return {
        type: SET_LIST_TASK,
        payload: data
    }
}
export const editListTask = (data) => {
    return {
        type: EDIT_LIST_TASK,
        payload: data
    }
}
export const setDisplayingTask = (data) => {
    return {
        type: SET_DISPLAY_TASK,
        payload: data
    }
}
