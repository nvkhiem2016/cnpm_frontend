import callApi from "./../utils/callApi";
import { GET_TASKS, NEW_TASK, EDIT_TASK, SET_NULL_EDIT_TASK,EDIT_STATUS_TASK, SET_EDIT_TASK } from "./../constants/types";
import { API_GET_TASKS, API_POST_TASK, API_PUT_TASK, API_PUT_STATUS_TASK, API_FINISH_TASK } from "./../constants/Config";
import {  editListTask } from "./calendarAction";
import openNotification from "../components/common/Notification";
import { task_message } from "../constants/message";
import moment from 'moment'
export const fetchTasksReq = () => {
  return (dispatch) => {
    return callApi(API_GET_TASKS, 'GET', null).then(res => {
      dispatch(fetchTasks(res.data.result))
    }).catch(err => {
      console.log(err)
    })
  }
}
export const fetchTasks = (tasks) => {
  return {
    type: GET_TASKS,
    payload: tasks
  }
}
export const newTaskReq = (task) => {
  return (dispatch) => {
    return callApi(API_POST_TASK, 'POST', task).then(res => {
      openNotification({type:"success",
      nameTask:task.name,
      description:task_message.NEW_TASK_SUCCESSFULLY})
      
      dispatch(newTask(res.data.result))//Gọi tạm api, về sau sẽ setState sẽ load nhanh hơn
    }).catch(err => {
      console.log(err)
    })
  }
}
export const newTask = (task) => {
  return {
    type: NEW_TASK,
    payload: task
  }
}

export const editTask = (task) => {
  return {
    type: EDIT_TASK,
    payload: task
  }
}

export const editTaskReq = (task) => {
  return (dispatch) => {
    return callApi(`${API_PUT_TASK}/${task.id}`, 'PUT', task).then(res => {
      openNotification({type:"success",
      nameTask:task.name,
      description:task_message.EDIT_TASK_SUCCESSFULLY})
      dispatch(setEditTask(res.data.result))
      dispatch(editListTask(res.data.result))
    }).catch(err => {
      console.log(err)
    })
  }
}
export const setEditTask = (task) => {
  return {
    type: SET_EDIT_TASK,
    payload: task
  }
}
export const setNullEditTask = (task) => {
  return {
    type: SET_NULL_EDIT_TASK,
    payload: task
  }
}
export const editStatusTaskReq = (task,status) => {
  let createAt = moment().format('YYYY-MM-DD HH:mm');//dùng để lưu lịch sử
  let data = {
    status:status,
    oldStatus:task.status,
    createAt:createAt,
  }
  return (dispatch) => {
    return callApi(`${API_PUT_STATUS_TASK}/${task.id}`, 'PUT', data).then(res => {
      task.status = status;
      dispatch(editStatusTask(task))
    }).catch(err => {
      console.log(err)
    })
  }
}
export const editStatusTask = (task) => {
  return {
    type: EDIT_STATUS_TASK,
    payload: task
  }
}
export const finishTaskReq = (task) => {
  let finishAt = moment().format('YYYY-MM-DD HH:mm');//dùng để lưu lịch sử
  return (dispatch) => {
    return callApi(`${API_FINISH_TASK}/${task.id}`, 'PUT', {finishAt}).then(res => {
      
    }).catch(err => {
      console.log(err)
    })
  }
}