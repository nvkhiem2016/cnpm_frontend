import {SET_SOCKET } from "./../constants/types";


export const setSocket = (socket) => {
    return {
        type: SET_SOCKET,
        payload: socket
    }
}