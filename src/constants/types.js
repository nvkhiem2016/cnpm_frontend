export const GET_ERRORS = 'GET_ERRORS';
export const SET_CURRENT_USER = 'SET_CRRENT_USER';
export const CLEAR_CURRENT_PROFILE = 'CLEAR_CURRENT_PROFILE';
export const PROFILE_LOADING = 'PROFILE_LOADING';
//user
export const GET_PROFILE = 'GET_PROFILE';
export const GET_USERS = 'GET_USERS';
export const CREATE_USER = 'CREATE_USER';
export const ADD_USER_ONLINE = 'ADD_USER_ONLINE';
export const REMOVE_USER_ONLINE = 'REMOVE_USER_ONLINE';


//Task
export const GET_TASKS = 'GET_TASKS';
export const NEW_TASK = 'NEW_TASK';
export const EDIT_TASK = 'EDIT_TASK';
export const SET_EDIT_TASK = 'SET_EDIT_TASK';
export const SET_NULL_EDIT_TASK = 'SET_NULL_EDIT_TASK';
export const EDIT_STATUS_TASK = 'EDIT_STATUS_TASK';


//Project
export const GET_MY_PROJECT = 'GET_MY_PROJECT';
export const GET_ALL_PROJECT = 'GET_ALL_PROJECT';
export const NEW_PROJECT = 'NEW_PROJECT';
export const ADD_USER_PROJECT = 'ADD_USER_PROJECT';
export const GET_TASKS_PROJECT = 'GET_TASK_PROJECT';




//Calendar
export const SET_LIST_TASK = 'SET_LIST_TASK';
export const EDIT_LIST_TASK = 'EDIT_LIST_TASK';

export const SET_DISPLAY_TASK = 'SET_DISPLAY_TASK';

//socket
export const SET_SOCKET = 'SET_SOCKET';
//reminder
export const SET_REMINDER = 'SET_REMINDER';
export const PUSH_REMINDER = 'PUSH_REMINDER';
export const EDIT_REMINDER = 'EDIT_REMINDER';
export const READ_REMINDER = 'READ_REMINDER';

//comment
export const NEW_COMMENT = 'NEW_COMMENT';
export const GET_ALL_COMMENT = 'GET_ALL_COMMENT';

//history
export const NEW_HISTORY = 'NEW_HISTORY';
export const GET_ALL_HISTORY = 'GET_ALL_HISTORY';

//file
export const NEW_FILE = 'NEW_FILE';
export const GET_FILES = 'GET_FILES';
 






