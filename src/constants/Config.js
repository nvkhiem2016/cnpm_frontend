export const API_URL = 'http://localhost:4000/api/v1';
//Tasks
export const API_GET_TASKS = `task/getAllTask`;
export const API_POST_TASK = `task`;
export const API_PUT_TASK = `task`;
export const API_PUT_STATUS_TASK = `task/setStatusTask`;
export const API_FINISH_TASK = `task/finishTask`;



//project
export const API_GET_MY_PROJECT = `project/getAllProjectsByUser`;
export const API_GET_ALL_PROJECT = `project/getAllProjects`;
export const API_POST_PROJECT = `project`;
export const API_ADD_USER_PROJECT = `project/addUser`;
export const API_GET_TASK_PROJECT = `task/getAllTaskByProjectId`;




//user - profile
export const API_GET_USERS = `user`;
export const API_CREATE_USER = `user`;
export const API_GET_PROFILE = `user/getProfile`;
//reminder
export const API_GET_REMINDER = `reminder`;
export const API_POST_REMINDER = `reminder`;
export const API_PUT_REMINDER = `reminder`;
export const API_READ_REMINDER = `reminder/readReminder`;
//comment
export const API_NEW_COMMENT = `comment`;
export const API_GET_COMMENT = `comment`;

//history
export const API_NEW_HISTORY = `history`;
export const API_GET_HISTORY = `history`;

//file
export const API_NEW_FILE = `file`;
export const API_GET_FILE = `file`;
export const API_DOWNLOAD_FILE = `file/download/downloadFile`;








