export const task_message = {
    NEW_TASK_SUCCESSFULLY:'Create new task successfully',
    EDIT_TASK_SUCCESSFULLY:'Edit task successfully',
}
export const project_message = {
    NEW_PROJECT_SUCCESSFULLY:'Create new project successfully',
    ADD_USER_SUCCESSFULLY:'Add user into project successfully',

}
export const reminder_message = {
    NEW_REMINDER_SUCCESSFULLY:'Create new reminder successfully',
    EDIT_REMINDER_SUCCESSFULLY:'Edit reminder successfully',

}

export const user_message = {
    CREATE_USER_SUCCESSFULLY:'Create create user successfully',
     

}
